package strings;

public class UnderstandingStrings {
    public static void main(String[] args) {
        String s1; // declaration of s1 as a String

        s1 = "TechGlobal School"; // initializing s1 as TechGlobal School
        String s2 = "is the best";  // declaration of s2 and initialization of s2 as is the best



        String s7; // this is a declaration os s7 as a String

        s7 = "initializing";

        String s6 = "declaration of s6 and initialization of s6 as this sentence";

        String s8 = s7 + " " + s6;

        System.out.println(s8);





        System.out.println("--------CONCAT USING + ----------\n");


        String s3 = s1 + " " + s2;  // concatenation using plus sign
        System.out.println(s3);  // TechGlobal School is the best

        System.out.println("\n--------CONCAT USING METHOD ----------\n");

        String s4 = s1.concat(" ").concat(s2);
        System.out.println(s4);


    }
}