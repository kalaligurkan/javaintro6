package strings;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        String is a reference type (object) that is used to store a sequence of characters
         TEXTS
         string starts with uppercase and use double quotation mark
         */

        String name= "John";
        String address = "Chicago IL 12345";

        System.out.println(name); // John
        System.out.println(address); // Chicago IL 12345

        String favMovie = "abcde";

        System.out.println("My favorite movie =" + " " + favMovie);
        System.out.println("My favorite movie = " +  favMovie);
    }
}
