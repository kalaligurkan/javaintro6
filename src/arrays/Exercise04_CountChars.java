package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {
        System.out.println("------1st way------"); //toCharArray()

        String word = ScannerHelper.getString();
        char[] charsOfWord = word.toCharArray();

        int letters = 0;
        for(char c : charsOfWord){
            if(Character.isLetter(c)) letters++;
        }
        System.out.println(letters);

        System.out.println("-------2nd way-------");
        String str = ScannerHelper.getString();
        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if(Character.isLetter(str.charAt(i))) counter++;

        }
        System.out.println(counter);

        System.out.println("\n-------2nd way------\n");  // toCharArray()

        letters = 0;

        for (char c : ScannerHelper.getString().toCharArray()) {
            if(Character.isLetter(c)) letters++;
        }

        System.out.println(letters);
    }
}
