package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {
        // 1. create an array to store -> 5.5, 6, 10.3, 25
        double[] doubleNumbers = {5.5, 6, 10.3, 25};

        // 2. print the array
        System.out.println(Arrays.toString(doubleNumbers));

        // 3. Print the size of the array -> The length is 4

            System.out.println("The length is " + doubleNumbers.length);

        // 4. Print each element using for each loop

        for (double number : doubleNumbers) {
            System.out.println(number);

        }

    }
}
