package arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {

        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

/*
Check the collection you have above and print true if it contains Mouse
Print false otherwise

RESULT:
true
*/
        boolean hasMouse = false;
        for (String object : objects) {
            if(object.equals("Mouse")) {
                hasMouse = true;
                break;
            }
        }
        System.out.println(hasMouse);
    }
}
