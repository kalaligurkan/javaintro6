package methods;

import utilities.ScannerHelper;

public class CalculateAge {
    public static void main(String[] args) {


        int anAge = ScannerHelper.getAge();
        int aNumber = ScannerHelper.getNumber();


        System.out.println("Age will be " + (anAge + aNumber) + " after "
                + aNumber + " years.");
    }
}
