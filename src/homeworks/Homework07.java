package homeworks;

import java.util.ArrayList;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        ArrayList<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(23);
        list.add(67);
        list.add(23);
        list.add(78);

        System.out.println(list.get(3));
        System.out.println(list.get(0));
        System.out.println(list.get(2));
        System.out.println(list);

        System.out.println("\n---------------TASK-2---------------\n");

        ArrayList<String> colors = new ArrayList<>();
        colors.add("Blue");
        colors.add("Brown");
        colors.add("Red");
        colors.add("White");
        colors.add("Black");
        colors.add("Purple");

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n---------------TASK-3---------------\n");

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(23);
        numbers.add(-34);
        numbers.add(-56);
        numbers.add(0);
        numbers.add(89);
        numbers.add(100);

        System.out.println(numbers);
        Collections.sort(numbers);
        System.out.println(numbers);

        System.out.println("\n---------------TASK-4---------------\n");

        ArrayList<String> cities = new ArrayList<>();
        cities.add("Istanbul");
        cities.add("Berlin");
        cities.add("Madrid");
        cities.add("Paris");

        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n---------------TASK-5---------------\n");

        ArrayList<String> marvel = new ArrayList<>();
        marvel.add("Spider Man");
        marvel.add("Iron Man");
        marvel.add("Black Panther");
        marvel.add("Deadpool");
        marvel.add("Captain America");

        System.out.println(marvel);
        System.out.println(marvel.contains("Wolverine"));

        System.out.println("\n---------------TASK-6---------------\n");

        ArrayList<String> avengers = new ArrayList<>();
        avengers.add("Hulk");
        avengers.add("Black Widow");
        avengers.add("Captain America");
        avengers.add("Iron Man");

        Collections.sort(avengers);
        System.out.println(avengers);
        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));

        System.out.println("\n---------------TASK-7---------------\n");

        ArrayList<Character> chars = new ArrayList<>();
        chars.add('A');
        chars.add('x');
        chars.add('$');
        chars.add('%');
        chars.add('9');
        chars.add('*');
        chars.add('+');
        chars.add('F');
        chars.add('G');

        System.out.println(chars);

        for (Character i : chars) {
            System.out.println(i);
        }

        System.out.println("\n---------------TASK-8---------------\n");

        ArrayList<String> objects = new ArrayList<>();
        objects.add("Desk");
        objects.add("Laptop");
        objects.add("Mouse");
        objects.add("Monitor");
        objects.add("Mouse-Pad");
        objects.add("Adapter");

        System.out.println(objects);
        Collections.sort(objects);
        System.out.println(objects);

        int countM = 0;
        int countAe = 0;

        for(String j : objects){
            if(j.contains("M") || j.contains("m")) countM++;
            if(!j.contains("A") && !j.contains("a") && !j.contains("E") && !j.contains("e")) countAe++;
        }
        System.out.println(countM);
        System.out.println(countAe);

        System.out.println("\n---------------TASK-9---------------\n");

        ArrayList<String> kitchen = new ArrayList<>();
        kitchen.add("Plate");
        kitchen.add("spoon");
        kitchen.add("fork");
        kitchen.add("Knife");
        kitchen.add("cup");
        kitchen.add("Mixer");

        System.out.println(kitchen);
        int upper = 0;
        int lower = 0;
        int wordP = 0;
        int startsP = 0;
        for(String kit : kitchen){
            if(Character.isUpperCase(kit.charAt(0))) upper++;
            else lower++;
        }
        for(String kit : kitchen) {
            if(kit.contains("P") || kit.contains("p")) {
                wordP++;
            }
        }
        for(String kit : kitchen){
            if(kit.startsWith("P") || kit.startsWith("p") || kit.endsWith("P") || kit.endsWith("p"))
                startsP++;
        }
        System.out.println("Elements starts with uppercase = " + upper);
        System.out.println("Elements starts with lowercase = " + lower);
        System.out.println("Elements having P or p = " + wordP);
        System.out.println("Elements starting or ending with P or p = " + startsP);

        System.out.println("\n---------------TASK-10---------------\n");

        ArrayList<Integer> number = new ArrayList<>();
        number.add(3);
        number.add(5);
        number.add(7);
        number.add(10);
        number.add(0);
        number.add(20);
        number.add(17);
        number.add(10);
        number.add(23);
        number.add(56);
        number.add(78);

        System.out.println(number);

        int divide10 = 0;
        int evenMoreThan15 = 0;
        int oddLessThan20 = 0;
        int less15more50 = 0;
        System.out.println(number);

        for(int j : number){
            if(j % 10 == 0) divide10++;
            if(j % 2 == 0 && j > 15) evenMoreThan15++;
            if(j % 2 == 1 && j < 20) oddLessThan20++;
            if(j < 15 || j > 50) less15more50++;
        }

        System.out.println("Elements that can be divided by 10 = " + divide10);
        System.out.println("Elements that even and greater than 15 = " + evenMoreThan15);
        System.out.println("Elements that odd and less than 20 = " + oddLessThan20);
        System.out.println("Elements that less than 15 or greater than 50 = " + less15more50);


        System.out.println("\n End of the program\n");





    }
}
