package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        String result = " ";
        for (int i = 1; i <= 100; i++) {

            if(i % 7 == 0) System.out.print(i + " - ");

        }


        System.out.println("\n---------------TASK-2---------------\n");

        for(int i = 1; i <= 50; i++){
            if(i % (2 * 3) == 0) System.out.print(i + " - ");
        }

        System.out.println("\n---------------TASK-3---------------\n");

        for(int i = 100; i >= 50; i--){
            if(i % 5 == 0) System.out.print(i + " - ");
        }

        System.out.println("\n---------------TASK-4---------------\n");

        for(int i = 0; i <= 7; i++){
            System.out.println("The square of " + i + " is = " + (i * i));
        }

        System.out.println("\n---------------TASK-5---------------\n");

        int sum = 0;
        for(int i = 1; i <= 10; i ++){
            sum += i;
        }
        System.out.println(sum);


        System.out.println("\n---------------TASK-6---------------\n");

        int number = ScannerHelper.getNumber();
        int fact = 1;
        for(int i = 1; i <= number; i++){
            fact = fact * i;
        }
        System.out.println(fact);

        System.out.println("\n---------------TASK-7---------------\n");

        String fullName = ScannerHelper.getFirstName().toLowerCase();
        int count = 0;
        for (int i = 0; i < fullName.length(); i++) {
            if(fullName.charAt(i)  == 'a' || fullName.charAt(i)  == 'e' || fullName.charAt(i)  == 'i'
            || fullName.charAt(i)  == 'o' || fullName.charAt(i)  == 'u') count++;

        }
        System.out.println("There are " + count + " vowel letters in this full name");

        System.out.println("\n---------------TASK-8---------------\n");

        String name;

        do {
            name = ScannerHelper.getFirstName().toLowerCase();
        } while (name.charAt(0) != 'j');

        System.out.println("End of the program");


        System.out.println("\nEnd of the program\n");


        }

    }

