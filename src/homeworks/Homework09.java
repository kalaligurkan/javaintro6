package homeworks;

import java.util.ArrayList;
import java.util.Arrays;


public class Homework09 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        int[] numbers = {-4, 0, -7, 0, 5, 10, 45, 45};
        System.out.println(firstDuplicate(numbers));

        System.out.println("\n---------------TASK-2---------------\n");

        String[] str = {"Z", "abc", "z", "123", "#"};
        System.out.println(firstDuplicatedString(str));

        System.out.println("\n---------------TASK-3---------------\n");
        int[] num = {0, -4, -7, 0, 5, 10, 45, -7, 0};
        System.out.println(duplicatedNumbers(num));

        System.out.println("\n---------------TASK-4---------------\n");
        String[] str1 = {"A", "foo", "12", "Foo", "bar", "a", "a"};
        System.out.println(duplicatedString(str1));

        System.out.println("\n---------------TASK-5---------------\n");
        String[] words1 = {"abc", "foo", "bar", "123", "java", "pyhton", "ruby"};
        System.out.println(reversedArray(words1));

        System.out.println("\n---------------TASK-6---------------\n");
        String words2 = ("Today is a fun day");
        System.out.println(reverseStringWords(words2));

    }


    //---------------TASK-1---------------

    public static int firstDuplicate(int[] numbers) {

        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[i] == numbers[j]) {
                    return numbers[i];
                }
            }
        }
        return -1;

    }


    //---------------TASK-2---------------

    public static String firstDuplicatedString(String[] str) {

        for (int i = 0; i < str.length; i++) {
            for (int j = i + 1; j < str.length; j++) {
                if (str[i].equalsIgnoreCase(str[j])) {
                    return str[i];

                }
            }
        }
        return "There is no duplicates";

    }


    //---------------TASK-3---------------

    public static ArrayList<Integer> duplicatedNumbers(int[] num) {
        ArrayList<Integer> dup = new ArrayList<>();

        for (int i = 0; i < num.length; i++) {
            for (int j = i + 1; j < num.length; j++) {
                if (num[i] == num[j] && !dup.contains(num[i])) {
                    dup.add(num[i]);
                }
            }
        }
        return dup;
    }
    //---------------TASK-4---------------

    public static ArrayList<String> duplicatedString(String[] str) {
        ArrayList<String> container = new ArrayList<>();

        for (int i = 0; i < str.length; i++) {
            for (int j = i + 1; j < str.length; j++) {
                if (str[i].equalsIgnoreCase(str[j]) && !container.contains(str[i].toLowerCase())) {
                    container.add(str[i].toLowerCase());
                }
            }
        }
        return container;
    }


    //---------------TASK-5---------------

    public static ArrayList<String> reversedArray(String[] str) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = str.length - 1; i >= 0; i--) {
            list.add(str[i]);
        }
        return list;
    }

    //---------------TASK-6---------------

    public static String reverseStringWords(String word) {
        String str = "";
        String[] strNew = word.split(" ");

        for (int i = 0; i < strNew.length; i++) {
            String word3 = strNew[i];

            String revWord = "";

            for (int j = word3.length()-1; j >= 0 ; j--) {
                revWord += word3.charAt(j);
            }
            str += revWord + " ";
        }
        return str;
    }
}











