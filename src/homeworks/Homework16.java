package homeworks;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Homework16 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");
        System.out.println(parseData("{105}LA{104}Paris{107}Berlin{101}Chicago{108}London"));


        System.out.println("\n---------------TASK-2---------------\n");
        HashMap<String, Integer> basket = new HashMap<>();
        basket.put("Orange", 3);
        basket.put("Mango", 1);
        basket.put("Apple", 2);
        System.out.println(calculateTotalPrice1(basket));

        System.out.println("\n---------------TASK-3---------------\n");
        HashMap<String,Integer> fruit = new HashMap<>();
        fruit.put("Apple", 4);
        fruit.put("Mango", 4);
        fruit.put("Orange", 9);
        System.out.println(calculateTotalPrice2(fruit));
    }


    //---------------TASK-1---------------
    public static HashMap<Integer, String> parseData(String str) {
        HashMap<Integer, String> returnEntries = new HashMap<>();

        String[] arr = str.split("[{}]");

        for (int i = 1; i < arr.length; i++) {
            returnEntries.put(Integer.parseInt(arr[i]), arr[i + 1]);
            i++;
        }
        return returnEntries;
    }

    //---------------TASK-2---------------
    public static double calculateTotalPrice1(Map<String, Integer> items) {
        double totalPrice = 0.0;

        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            String fruit = entry.getKey();
            int amount = entry.getValue();

            switch (fruit) {
                case "Apple":
                    totalPrice += 2.00 * amount;
                    break;
                case "Orange":
                    totalPrice += 3.29 * amount;
                    break;
                case "Mango":
                    totalPrice += 4.99 * amount;
                    break;
                case "Pineapple":
                    totalPrice += 5.25 * amount;
                    break;
            }
        }

        return totalPrice;
    }

    //---------------TASK-3---------------

    public static double calculateTotalPrice2(HashMap<String, Integer> fruit) {
        double price = 0;

        for (String str : fruit.keySet()) {
            switch (str) {
                case "Apple":
                    if (fruit.get("Apple") % 2 == 0) price += fruit.get("Apple") * 1.50;
                    else price += (fruit.get("Apple") * 1.50) + .50;
                    break;
                case "Mango":
                    int freeMango = fruit.get("Mango") / 4;
                    price += (fruit.get("Mango") * 4.99) - (freeMango);
                    break;
                case "Orange":
                    price += fruit.get("Orange") * 3.29;
                    break;
            }
        }
        return price;

    }
}
