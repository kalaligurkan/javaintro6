package homeworks;

public class Homework18 {
    public static void main(String[] args){
        System.out.println("\n---------------TASK-1---------------\n");

        System.out.println("\n---------------TASK-2---------------\n");

        System.out.println(splitString("Hello", 3));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println(countPalindrome("Mom and Dad"));
    }


    //---------------TASK-1---------------
    public static int[] doubleOrTriple(int[] arr, boolean doubleValue){
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i];

        }
        return result;
    }

    //---------------TASK-2---------------
    public static String splitString(String str, int lengthOfSplit){
        if (str.length() % lengthOfSplit != 0){
            return "";
        }

        String result = "";
        int firstIndex = 0;
        int lastIndex = lengthOfSplit;

        while (lastIndex <= str.length()){
            result += str.substring(firstIndex, lastIndex) + " ";
            firstIndex += lastIndex;
            lastIndex += lengthOfSplit;
        }

        return result;
    }

    //---------------TASK-3---------------
    public static int countPalindrome(String str){
        String[] words = str.split(" ");
        int count = 0;

        for (String word : words) {
            if(isPalindrome(word)){
                count++;
            }
        }
        return count;
    }

    public static boolean isPalindrome(String str1){
        str1 = str1.toLowerCase();
        String container = "";
        for (int i = str1.length()-1; i >= 0; i--) {
            container += str1.charAt((i));
        }
        if(container.equals(str1)){
            return true;
        }
        return false;
    }
}
