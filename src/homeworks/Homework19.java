package homeworks;

import java.util.Arrays;

public class Homework19 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");
        int[] arr = {1, 5, 10};
        boolean index = true;
        System.out.println(sum(arr, index));

        System.out.println("\n---------------TASK-2---------------\n");
        String str = "Java";
        int num = 2;
        System.out.println(nthChars(str, num));

        System.out.println("\n---------------TASK-3---------------\n");
        String str1 = "Hello";
        String str2 = "Hi";
        System.out.println(canFormString(str1, str2));

        System.out.println("\n---------------TASK-4---------------\n");
        String str3 = "Apple";
        String str4 = "Peach";
        System.out.println(isAnagram(str3, str4));

        String str5 = "astronomer";
        String str6 = "moon starer";
        System.out.println(isAnagram(str5, str6));
    }


    //---------------TASK-1---------------
        public static int sum(int[] arr, boolean index) {
            int sum = 0;

            if (index) {
                for (int i = 0; i < arr.length; i += 2) {
                    sum += arr[i];
                }
            } else {
                for (int i = 1; i < arr.length; i += 2) {
                    sum += arr[i];
                }
            }
            return sum;
        }

    //---------------TASK-2---------------
    public static String nthChars(String input, int n) {
        if (input.length() < n) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        int index = 0;

        while (index < input.length()) {
            result.append(input.charAt(index));
            index += n;
        }
        return result.toString();
    }

    //---------------TASK-3---------------
    public static boolean canFormString(String str1, String str2) {
        str1 = str1.replaceAll("\\s", "").toLowerCase();
        str2 = str2.replaceAll("\\s", "").toLowerCase();

        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();
        Arrays.sort(arr1);
        Arrays.sort(arr2);

        return Arrays.equals(arr1, arr2);
    }

    //---------------TASK-4---------------
    public static String removeWhiteSpaces(String str) {
        StringBuilder sb = new StringBuilder();
        for (char c : str.toCharArray()) {
            if (!Character.isWhitespace(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    public static boolean isAnagram(String str1, String str2) {
        str1 = removeWhiteSpaces(str1).toLowerCase();
        str2 = removeWhiteSpaces(str2).toLowerCase();

        char[] arr1 = str1.toCharArray();
        char[] arr2 = str2.toCharArray();
        Arrays.sort(arr1);
        Arrays.sort(arr2);

        return Arrays.equals(arr1, arr2);
    }
}




