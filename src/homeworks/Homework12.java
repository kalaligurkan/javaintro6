package homeworks;

import java.util.Arrays;

public class Homework12 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");

        System.out.println(noDigit("123Java hello 123"));

        System.out.println("\n---------------TASK-2---------------\n");

        System.out.println(noVowel("TechGlobal"));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println(sumOfDigits("John's age is 29"));

        System.out.println("\n---------------TASK-4---------------\n");

        System.out.println(hasUpperCase("jaVa"));

        System.out.println("\n---------------TASK-5---------------\n");

        System.out.println(middleInt(5,3,5));

        System.out.println("\n---------------TASK-6---------------\n");

        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3, 4, 0, 5, 13, 9})));

        System.out.println("\n---------------TASK-7---------------\n");

        System.out.println(Arrays.toString(arrFactorial(new int[]{1, 2, 3, 4, 5})));

        System.out.println("\n---------------TASK-8---------------\n");

        //System.out.println(Arrays.toString(categorizeCharacters("abc123$#%")));
    }

    //---------------TASK-1---------------
    public static String noDigit(String str){
        return str.replaceAll("\\d", "");
    }

    //---------------TASK-2---------------
    public static String noVowel(String str){
        return str.replaceAll("[aeiouAEIOU]", "");
    }

    //---------------TASK-3---------------
    public static int sumOfDigits(String str){
        int sum = 0;

        for (int i = 0; i < str.length(); i++) {
            if(Character.isDigit(str.charAt(i))){
                sum += Character.getNumericValue(str.charAt(i));
            }
        }
        return sum;
    }

    //---------------TASK-4---------------
    public static boolean hasUpperCase(String str){
        for (int i = 0; i < str.length(); i++) {
            if(Character.isUpperCase(str.charAt(i)))
                return true;
        }
        return false;
    }

    //---------------TASK-5---------------
    public static int middleInt(int num1, int num2, int num3){
        int[] arrayNum = new int[]{num1, num2, num3};
        Arrays.sort(arrayNum);
        return arrayNum[arrayNum.length/2];
    }

    //---------------TASK-6---------------
    public static int[] no13(int[] arrNumber){
        for (int i = 0; i < arrNumber.length; i++) {
            if(arrNumber[i] == 13)
                arrNumber[i] = 0;
        }
        return arrNumber;
    }

    //---------------TASK-7---------------   check
    public static int[] arrFactorial(int[] arrNum){
        for (int i = 0; i < arrNum.length; i++) {
            for (int j = i + 1; j < arrNum.length; j++) {
                arrNum[j] *= arrNum[i];
            }
        }
        return  arrNum;
    }

    //---------------TASK-8---------------  check
    //public static String[] categorizeCharacters(String str) {}







}
