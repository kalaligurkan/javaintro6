package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        int[] numbers = new int[10];
        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n---------------TASK-2---------------\n");

        String[] words = new String[5];
        words[1] = "abc";
        words[4] = "xyz";
        System.out.println(words[3]);
        System.out.println(words[0]);
        System.out.println(words[4]);
        System.out.println(Arrays.toString(words));

        System.out.println("\n---------------TASK-3---------------\n");

        int[] numbers1 = {23, -34, -56, 0, 89, 100};
        System.out.println(Arrays.toString(numbers1));
        Arrays.sort(numbers1);
        System.out.println(Arrays.toString(numbers1));

        System.out.println("\n---------------TASK-4---------------\n");

        String[] words1 = {"Germany", "Argentina", "Ukraine", "Romania"};
        System.out.println(Arrays.toString(words1));
        Arrays.sort(words1);
        System.out.println(Arrays.toString(words1));

        System.out.println("\n---------------TASK-5---------------\n");

        String[] cartoonDogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};
        System.out.println(Arrays.toString(cartoonDogs));
        boolean hasPluto = false;
        for (String cartoonDog : cartoonDogs) {
            if (cartoonDog.equals("Pluto")) {
                hasPluto = true;
                break;
            }
        }
        System.out.println(hasPluto);


        System.out.println("\n---------------TASK-6---------------\n");

        String[] cartoonCats = {"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(cartoonCats);
        System.out.println(Arrays.toString(cartoonCats));
        boolean haveCats = false;
        for (String cartoonCat : cartoonCats) {
            if (cartoonCat.equals("Garfiels") && cartoonCat.equals("Felix")) {
                haveCats = true;
                break;
            }
        }
        System.out.println(haveCats);

        System.out.println("\n---------------TASK-7---------------\n");

        double[] doubleNumbers = {10.5, 20.75, 70, 80, 15.75};
        System.out.println(Arrays.toString(doubleNumbers));
        for (double doubleNumber : doubleNumbers) {
            System.out.println(doubleNumber);
        }

        System.out.println("\n---------------TASK-8---------------\n");

        char[] chars = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(chars));

        int upperL = 0;
        int lowerL = 0;
        int digit = 0;
        int special = 0;

        for(char char1 : chars){
            if(Character.isUpperCase(char1)) upperL++;
            else if(Character.isLowerCase(char1)) lowerL++;
            else if(Character.isDigit(char1)) digit++;
            else if(!Character.isLetter(char1) &&
                    !Character.isUpperCase(char1) &&
                    !Character.isLowerCase(char1)&&
                    !Character.isDigit(char1)) special++;
        }
        System.out.println("Letters = " + (upperL + lowerL));
        System.out.println("Uppercase letters = " + upperL);
        System.out.println("Lowercase letters = " + lowerL);
        System.out.println("Digits = " + digit);
        System.out.println("Special characters = " + special);

        System.out.println("\n---------------TASK-9---------------\n");

        String[] words9 = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        System.out.println(Arrays.toString(words9));
        int upperE = 0;
        int lowerE = 0;
        int bAndP = 0;
        int bookPen = 0;
        for(String word : words9) {
            if (Character.isUpperCase(word.charAt(0))) upperE++;
            else lowerE++;
            if (word.toLowerCase().charAt(0) == 'b' || word.toLowerCase().charAt(0) == 'p') bAndP++;
            if (word.toLowerCase().contains("book") || word.toLowerCase().contains("pen")) bookPen++;
        }
            System.out.println("Elements starts with uppercase = " + upperE);
            System.out.println("Elements starts with lowercase = " + lowerE);
            System.out.println("Elements starts with B or P = " + bAndP);
            System.out.println("Elements having \"book\" or \"pen\" = " + bookPen);


        System.out.println("\n---------------TASK-10---------------\n");

        int[] numbers10 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(numbers10));
        int more10 = 0;
        int less10 = 0;
        int have10 = 0;
        for(int n10 : numbers10){
            if(n10 > 10) more10++;
            else if(n10 <10) less10++;
            else if(n10 == 10 ) have10++;
        }
        System.out.println("Elements that are more than 10 = " + more10);
        System.out.println("Elements that are less than 10 = " + less10);
        System.out.println("Elements that are 10 = " + have10);

        System.out.println("\n---------------TASK-11---------------\n");

        int[] first = {5, 8, 13, 1, 2};
        int[] second = {9, 3, 67, 1, 0};
        System.out.println("1st array is = "+ Arrays.toString(first));
        System.out.println("2nd array is = " + Arrays.toString(second));

        int[] third = new int[5];
        for(int i = 0; i < third.length; i++){
            third[i] = Math.max(first[i], second[i]);
        }
        System.out.println("3rd array is = " + Arrays.toString(third));


        System.out.println("End of the Program");





    }
}


