package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 2 numbers:");

        int number1 = input.nextInt();
        int number2 = input.nextInt();
        input.nextLine();

        System.out.println("The difference between numbers is = " + Math.abs(number1 - number2));


        System.out.println("\n---------------TASK-2---------------\n");

        System.out.println("Please enter 5 numbers:");

        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();
        int num4 = input.nextInt();
        int num5 = input.nextInt();
        input.nextLine();

        System.out.println("Max value = " + Math.max(num1, (Math.max(Math.max(num2, num3), Math.max(num4, num5)))));


        System.out.println("Min value = " + Math.min(num1, (Math.min(Math.min(num2, num3), Math.min(num4, num5)))));


        System.out.println("\n---------------TASK-3---------------\n");

        int n1 = (int) (Math.random() * 51) + 50;
        int n2 = (int) (Math.random() * 51) + 50;
        int n3 = (int) (Math.random() * 51) + 50;


        System.out.println("Number 1 = " + n1);
        System.out.println("Number 2 = " + n2);
        System.out.println("Number 3 = " + n3);


        System.out.println("The sum of numbers is = " + (n1 + n2 + n3));


        System.out.println("\n---------------TASK-4---------------\n");

        double alex = 125;
        double mike = 220;
        alex -= 25.5;
        mike += 25.5;

        System.out.println("Alex's money: $" + alex);
        System.out.println("Mike's money: " + mike);


        System.out.println("\n---------------TASK-5---------------\n");

        double bicycle = 390;
        double dailySave = 15.60;
        int day = (int) (bicycle / dailySave);

        System.out.println(day);


        System.out.println("\n---------------TASK-6---------------\n");

        String s1 = "5", s2 = "10";

        int num01 = Integer.parseInt(s1);
        int num02 = Integer.parseInt(s2);

        System.out.println("Sum of 5 and 10 is = " + (num01 + num02));
        System.out.println("Product of 5 and 10 is = " + num01 * num02);
        System.out.println("Division of 5 and 10 is = " + num01 / num02);
        System.out.println("Subtraction of 5 and 10 is = " + (num01 - num02));
        System.out.println("Remainder of 5 and 10 is = " + num01 % num02);


        System.out.println("\n---------------TASK-7---------------\n");


        String str1 = "200", str2 = "-50";

        int no1 = Integer.parseInt(str1);
        int no2 = Integer.parseInt(str2);

        System.out.println("The greatest value is = " + Math.max(no1, no2));
        System.out.println("The smallest value is = " + Math.min(no1, no2));
        System.out.println("The average value is = " + (no1 + no2) / 2);
        System.out.println("The absolute difference is = " + Math.abs(no1-no2));


        System.out.println("\n---------------TASK-8---------------\n");


        double quarter = (3 * 0.25);
        double dime = (0.10);
        double nickel = (2 * 0.05);
        double penny = (0.01);

        double savePerDay = quarter + dime + nickel + penny;

        System.out.println((int) (24 / savePerDay) + " days");
        System.out.println((int) (168 / savePerDay) + " days");
        System.out.println("$" + 150 * savePerDay);


        System.out.println("\n---------------TASK-9---------------\n");

        double saving = 62.5;
        double computer = 1250;

        System.out.println((int) (computer / saving));


        System.out.println("\n---------------TASK-10---------------\n");

        double payment1 = 475.50;
        double payment2 = 951;
        double totalPayment = 14265;

        System.out.println("Option 1 will take " + (int) (totalPayment / payment1) + " months");
        System.out.println("Option 2 will take " + (int) (totalPayment / payment2) + " months");


        System.out.println("\n---------------TASK-11---------------\n");

        System.out.println("Please enter 2 number:");

        int numb1 = input.nextInt();
        int numb2 = input.nextInt();

        System.out.println((double) numb1 / numb2);


        System.out.println("\n---------------TASK-12---------------\n");


        int nr1 = ((int)(Math.random() * 101));
        int nr2 = ((int)(Math.random() * 101));
        int nr3 = ((int)(Math.random() * 101));

        if(nr1 > 25 && nr2 > 25 && nr3 > 25){
            System.out.println("True");
        }
        else {
            System.out.println("False");
        }


        System.out.println("\n---------------TASK-13---------------\n");

        System.out.println("Please enter a number between 1 to 7:");

        int number1T13 = input.nextInt();

        if(number1T13 == 1){
            System.out.println("The number entered returns MONDAY");
        } else if(number1T13 == 2){
            System.out.println("The number entered returns TUESDAY");
        } else if(number1T13 == 3){
            System.out.println("The number entered returns WEDNESDAY");
        } else if(number1T13 == 4){
            System.out.println("The number entered returns THURSDAY");
        } else if(number1T13 == 5){
            System.out.println("The number entered returns FRIDAY");
        } else if(number1T13 == 6){
            System.out.println("The number entered returns SATURDAY");
        } else{
            System.out.println("The number entered returns SUNDAY");
        }


        System.out.println("\n---------------TASK-14---------------\n");

        System.out.println("Tell me your exam results?");

        int result1 = input.nextInt();
        int result2 = input.nextInt();
        int result3 = input.nextInt();
        int avePoint = (result1 + result2 + result3) / 3;

        if(avePoint >= 70){
            System.out.println("YOU PASSED!");
        } else{
            System.out.println("YOU FAILED!");
        }


        System.out.println("\n---------------TASK-15---------------\n");

        System.out.println("Please enter 3 number:");

        int no1T15 = input.nextInt();
        int no2T15 = input.nextInt();
        int no3T15 = input.nextInt();

        if(no1T15 == no2T15 && no2T15 == no3T15){
            System.out.println("TRIPLE MATCH");
        } else if(no1T15 == no2T15 || no1T15 == no3T15 || no2T15 == no3T15){
            System.out.println("DOUBLE MATCH");
        } else{
            System.out.println("NO MATCH");
        }


        System.out.println("\nEnd of the program\n");






    }
}
