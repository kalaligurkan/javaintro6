package homeworks;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n---------TASK1---------\n");


        /*
        JAVA = 01001010010000010101011001000001

        SELENIUM = 0101001101000101010011000100010101001110010010010101010101001101
         */

        System.out.println("\n---------TASK2---------\n");


        /*
        01001101    = M
        01101000    = h
        01111001    = y
        01010011    = S
        01101100    = l
         */

        System.out.println("\n---------TASK3---------\n");


        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");


        System.out.println("\n---------TASK4---------\n");


        System.out.println("\tJava is easy to write and easy to run—this is the foundational\n" +
                "strength of Java and why many developers program in it. When you\n" +
                "write Java once, you can run it almost anywhere at any time." +
                "\n\n\tJava can be used to create complete applications that can run on\n" +
                "a single computer or be distributed across servers and clients in a\n" +
                "network.\n\n\tAs a result, you can use it to easily build mobile applications or\n" +
                "run-on desktop applications that use different operating systems and\n" +
                "servers, such as Linux or Windows.");


        System.out.println("\n---------TASK5---------\n");


        short myAge = 32;
        byte myFavoriteNumber = 13;
        String myHeight = "6'00";  // double myHeight = 6.00
        int myWeight = 165;
        char myFavoriteLetter = 'R';

        System.out.println(myAge);
        System.out.println(myFavoriteNumber);
        System.out.println(myHeight);
        System.out.println(myWeight);
        System.out.println(myFavoriteLetter);
    }
}
