package homeworks;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;

public class Homework11 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        String word1 = "   Hello   ";
        System.out.println(noSpace(word1));

        System.out.println("\n---------------TASK-2---------------\n");

        String word2 = "  Hello  ";
        System.out.println(replaceFirstLast(word2));

        System.out.println("\n---------------TASK-3---------------\n");

        String word3 = "123456";
        System.out.println(hasVowel(word3));

        System.out.println("\n---------------TASK-4---------------\n");

        checkAge(1809);

        System.out.println("\n---------------TASK-5---------------\n");

        System.out.println(averageOfEdges(0, 0, 0));
        System.out.println(averageOfEdges(0, 0, 6));
        System.out.println(averageOfEdges(-2, -2, 10));
        System.out.println(averageOfEdges(-3, 15, -3));
        System.out.println(averageOfEdges(10, 13, 20));

        System.out.println("\n---------------TASK-6---------------\n");

        String[] word6 = {"appium", "java", "hello", "Abc", "123", "xyz"};
        System.out.println(Arrays.toString(noA(word6)));

        System.out.println("\n---------------TASK-7---------------\n");

        int[] num7 = {7, 4, 11, 23, 17, 3, 4, 5, 6, 15, 30, 90};
        System.out.println(Arrays.toString(no3or5(num7)));

        System.out.println("\n---------------TASK-8---------------\n");

        int[] num8 = {41, 53, 19, 47, 67, 7, 4, 11, 23, 17};
        System.out.println(countPrimes(num8));
    }

    //---------------TASK-1---------------
    public static String noSpace(String str1) {

        return str1.trim().replaceAll("\\s", "");
    }

    //---------------TASK-2---------------

    public static String replaceFirstLast(String str2) {

        str2 = str2.trim().replaceAll("\\s+", "");

        if (str2.length() < 2) return "";
        else {
            char first = str2.charAt(0);
            String middle = str2.substring(1, str2.length() - 1);
            char last = str2.charAt(str2.length() - 1);
            return last + middle + first;
        }
    }

    //---------------TASK-3---------------

    public static boolean hasVowel(String str3) {
        if (str3 == null) return false;
        str3 = str3.toLowerCase();
        for (int i = 0; i < str3.length(); i++) {
            char c = str3.charAt(i);
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                return true;
            }

        }
        return false;
        /*
        if(str3 == null) return false;
        return str3.toLowerCase().matches(".*[aeiou].*"); 
        */
    }

    //---------------TASK-4---------------

    public static void checkAge(int yearOfBirth) {

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int age = currentYear - yearOfBirth;

        if (age > 0 && age < 16)
            System.out.println("AGE IS NOT ALLOWED");
        else if (age > 16 && age < 100)
            System.out.println("AGE IS ALLOWED");
        else
            System.out.println("AGE IS NOT VALID");
    }


    //---------------TASK-5---------------

    public static int averageOfEdges(int num1, int num2, int num3) {

        int min = (Math.min(Math.min(num1, num2), num3));
        int max = (Math.max(Math.max(num1, num2), num3));
        return (min + max) / 2;
    }

    //---------------TASK-6---------------

    public static String[] noA(String[] str) {
        String[] newStr = new String[str.length];
        for (int i = 0; i < str.length; i++) {
            if (str[i].toLowerCase().startsWith("a")) {
                newStr[i] = "###";
            } else {
                newStr[i] = str[i];
            }
        }
        return newStr;
    }

    //---------------TASK-7---------------

    public static int[] no3or5(int[] str) {
        int[] newInt = new int[str.length];
        for (int i = 0; i < str.length; i++) {
            if (str[i] % (3 * 5) == 0) {
                newInt[i] = 101;
            } else if (str[i] % 5 == 0) {
                newInt[i] = 99;
            } else if (str[i] % 3 == 0) {
                newInt[i] = 100;
            } else {
                newInt[i] = str[i];
            }
        }
        return newInt;
    }

    //---------------TASK-8---------------

    public static int countPrimes(int[] arr) {
        int noPrime = 0;

        for (int num : arr) {
            if (num < 2) {
                noPrime++;
                continue;
            }
            for (int i = 2; i < num; i++) {
                if (num % i == 0)
                    noPrime++;
            }
        }
        return arr.length - noPrime;
    }
}



