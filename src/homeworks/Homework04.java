package homeworks;

import utilities.ScannerHelper;

public class Homework04 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        String userName = ScannerHelper.getFirstName();
        System.out.println("The length of the name is = " + userName.length());

        System.out.println("The first character in the name is = " + userName.charAt(0));
        System.out.println("The last character in the name is = " + userName.charAt(userName.length()-1));
        System.out.println("The first 3 characters in the name is = " + userName.substring(0,3));
        System.out.println("The last 3 characters in the name is = " + userName.substring(2));
        if(userName.startsWith("A") || userName.startsWith("a"))
            System.out.println("You are in the club");
        else System.out.println("Sorry, you are not in the club");


        System.out.println("\n---------------TASK-2---------------\n");

        String userAddress = ScannerHelper.getAddress();
        if(userAddress.toLowerCase().contains("chicago"))
            System.out.println("You are in the club");
        else if(userAddress.toLowerCase().contains("des plaines"))
        System.out.println("You are welcome to join the club");
        else System.out.println("Sorry, you will never be in the club");


        System.out.println("\n---------------TASK-3---------------\n");

        String favCountry = ScannerHelper.getFavCountry();

        if(favCountry.toLowerCase().contains("a") && favCountry.toLowerCase().contains("i"))
            System.out.println("A and i are there");
        else if (favCountry.toLowerCase().contains("a")) System.out.println("A is there");
        else if (favCountry.toLowerCase().contains("i")) System.out.println("I is there");
        else System.out.println("A and i are not there");


        System.out.println("\n---------------TASK-4---------------\n");

        String str = " Java is FUN ";

        System.out.println("The first word in the str is = " + str.toLowerCase().trim().substring(0,4));
        System.out.println("The second word in the str is = " + str.toLowerCase().substring(5, 8));
        System.out.println("The third word in the str is = " + str.toLowerCase().substring(8));


        System.out.println("\nEnd of the program");




    }
}
