package homeworks;

import java.util.Arrays;

public class Homework15 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");
        System.out.println(Arrays.toString(fibonacciSeries1(0)));

        System.out.println("\n---------------TASK-2---------------\n");
        System.out.println(fibonacciSeries2(4));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println("\n---------------TASK-4---------------\n");
        System.out.println(firstDuplicate(new int[]{1, 2, 3, 4, 4}));

        System.out.println("\n---------------TASK-5---------------\n");
        System.out.println(isPowerOf3(80));
    }

    //---------------TASK-1---------------
    public static int[] fibonacciSeries1(int n){
        int num1;
        int num2 = 0;
        int num3 = 1;
        int[] sum = new int[n];

        for (int i = 0; i < n; i++) {
            sum[i] = num2;
            num1 = num2;
            num2 = num3;
            num3 = num1 + num3;
        }
        return sum;
    }

    //---------------TASK-2---------------
    public static int fibonacciSeries2(int n){
        int num1;
        int num2 = 0;
        int num3 = 1;
        int result = 1;

        for (int i = 0; i < n; i++) {
            result = num2;
            num1 = num2;
            num2 = num3;
            num3 = num1 + num3;
        }
        return result;
    }

    //---------------TASK-3---------------
   /* public static int[] findUniques(int[] arr, int[] arr1){

    } */

    //---------------TASK-4---------------
    public static int firstDuplicate(int[] arr){
        int[] result = new int[arr.length + 1];
        for (int i : arr) {
            if (result[i] == 1)
                return i;
            result[i]++;
        }
        return -1;
    }

    //---------------TASK-5---------------
    public static boolean isPowerOf3(int n){
        if(n <= 0) {
            return false;
        }
        while(n % 3 == 0){
            n /= 3;
        }
        return n == 1;
    }
}
