package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args){
        System.out.println("\n--------------------TASK-1--------------------\n");

        Scanner input = new Scanner (System.in);

        // number1 = 4, number2 = 9
        int number1, number2;

        System.out.println("Please enter 2 numbers:");

        number1 = input.nextInt();
        input.nextLine();
        number2 = input.nextInt();

        int sum = number1 + number2;

        System.out.println("The number 1 entered by user is = " + number1);
        System.out.println("The number 2 entered by user is = " + number2);
        System.out.println("The sum of number 1 and 2 entered by user is = " + sum);


        System.out.println("\n--------------------TASK-2--------------------\n");
        // num1 = 5, num2= 7

        int num1, num2;

        System.out.println("Please enter 2 numbers:");

        num1 = input.nextInt();
        num2 = input.nextInt();

        int product = num1 * num2;
        System.out.println("The product of the given 2 numbers is: " + product);

        System.out.println("\n--------------------TASK-3--------------------\n");
        // d1 = 24, d2 = 10

        double d1, d2;

        System.out.println("Please enter 2 numbers:");

        d1 = input.nextInt();
        d2 = input.nextInt();


        System.out.println("The sum of the given numbers is = " + (d1 + d2));
        System.out.println("The product of the given numbers is = " + (d1 * d2));
        System.out.println("The subtraction of the given numbers is = " + (d1 - d2));
        System.out.println("The division of the given numbers is = " + (d1 / d2));
        System.out.println("The remainder of the given numbers is = " + (d1 % d2));


        System.out.println("\n--------------------TASK-4--------------------\n");


        System.out.println("1." + "         " + (-10 + (7 * 5)));
        System.out.println("2." + "         " + (72 + 24) % 24);
        System.out.println("3." + "         " + (10 + -3 * 9 / 9));
        System.out.println("4." + "         " + (5 + 18 / 3 * 3 - (6 % 3)));


        System.out.println("\n--------------------TASK-5--------------------\n");
        // task5Number1 = 7, task5Number2 = 11

       int task5Number1, task5Number2;



        System.out.println("Please enter 2 numbers:");

        task5Number1 = input.nextInt();
        task5Number2 = input.nextInt();


        System.out.println("The average of the given numbers is: " + ((task5Number1 + task5Number2) / 2));


        System.out.println("\n--------------------TASK-6--------------------\n");
        // int task6Number1 = 6, task6Number2 = 10, task6Number3 = 12, task6Number4 = 15, task6Number5= 17

        int task6Number1, task6Number2, task6Number3, task6Number4, task6Number5;

        System.out.println("Please enter 5 numbers:");

        task6Number1 = input.nextInt();
        task6Number2 = input.nextInt();
        task6Number3 = input.nextInt();
        task6Number4 = input.nextInt();
        task6Number5 = input.nextInt();

        System.out.println("The average of the given numbers is: " + (task6Number1 + task6Number2 + task6Number3 +
                task6Number4 + task6Number5) / 5);


        System.out.println("\n--------------------TASK-7--------------------\n");


        System.out.println("Please enter 3 numbers:");

        int task7Number1 = input.nextInt(), task7Number2 = input.nextInt(), task7Number3 = input.nextInt();;
        int multiple1 = 5 * task7Number1;
        int multiple2 = 6 * task7Number2;
        int multiple3 = 10 * task7Number3;


        System.out.println("Please enter 3 numbers:");


        System.out.println("The 5 multiplied with 5 is = " + multiple1);
        System.out.println("The 6 multiplied with 6 is = " + multiple2);
        System.out.println("The 10 multiplied with 10 is = " + multiple3);

        System.out.println("\n--------------------TASK-8--------------------\n");


        System.out.println("Please enter a side of the square:");

        int task8Number = input.nextInt();
        input.nextLine();
        int perimeter = task8Number * 4;
        int area = task8Number * task8Number;

        System.out.println("Perimeter of the square = " + perimeter);
        System.out.println("Area of the square = " + area);

        System.out.println("\n--------------------TASK-9--------------------\n");


        double annualSalary = 90_000;
        double annualSalary3 = annualSalary * 3;


        System.out.println("A Software Engineer in Test can earn " + "$" + annualSalary3 + " in 3 years.");


        System.out.println("\n--------------------TASK-10--------------------\n");


        System.out.println("Please enter favorite book:");
        String favBook = input.nextLine();


        System.out.println("Please enter favorite color:");
        String favColor = input.nextLine();


        System.out.println("Please enter favorite number:");
        int favNumber = input.nextInt();
        input.nextLine();


        System.out.println("User's favorite book is: " + favBook +
                "\nUser's favorite color is: " + favColor +
                "\nUser's favorite number is: " + favNumber);

        System.out.println("\n--------------------TASK-11--------------------\n");


        System.out.println("Please enter your first name:");
        String firstName = input.nextLine();

        System.out.println("Please enter your last name:");
        String lastName = input.nextLine();

        System.out.println("Please enter your age:" );
        int age = input.nextInt();
        input.nextLine();

        System.out.println("Please enter your email address:");
        String emailAddress = input.nextLine();

        System.out.println("Please enter your phone number:");
        String phoneNumber = input.nextLine();

        System.out.println("Please enter your address:");
        String address = input.nextLine();




        System.out.println("\tUser who joined this program is " + firstName + " " + lastName + ". " + firstName + "'s" +
                " age is " + age + ". " + firstName + "'s email \naddress is " +
                emailAddress + ", phone number is " + phoneNumber + ", and address \nis " +
                address + ".");



    }
}
