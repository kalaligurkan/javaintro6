package homeworks;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        System.out.println(countConsonant("JAVA"));

        System.out.println("\n---------------TASK-2---------------\n");

        wordArray("Hello, nice to meet you!!");

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println(removeExtraSpaces("java    is      fun"));

        System.out.println("\n---------------TASK-4---------------\n");

        System.out.println(count3OrLess());

        System.out.println("\n---------------TASK-5---------------\n");

        System.out.println(isDateFormatValid("10/07/1849"));

        System.out.println("\n---------------TASK-6---------------\n");

        System.out.println(isEmailFormatValid("a@gmail.com"));
    }


    //---------------TASK-1---------------

    public static int countConsonant(String str){
        str = str.toLowerCase().replaceAll("[^bcdfghjklmnpqrstvwxyz]", "");
        return str.length();
    }

    //---------------TASK-2---------------

    public static String[] wordArray(String str){
        Pattern pattern = Pattern.compile("[a-zA-Z]{1,}");
        Matcher matcher = pattern.matcher(str);
        while(matcher.find()){
            System.out.println(matcher.group());
        }
        return new String[]{str};
    }

    //---------------TASK-3---------------

    public static String removeExtraSpaces(String str){
        str = str.replaceAll("\\s+", " ");
        str = str.trim();
        return str;
    }

    //---------------TASK-4---------------

    public static int count3OrLess(){
        String str = ScannerHelper.getSentence();
        Pattern pattern = Pattern.compile("\\b\\w{1,3}\\b");
        Matcher matcher = pattern.matcher(str);
        int count = 0;
        while (matcher.find()){
            count++;
        }
        return count;
    }

    //---------------TASK-5---------------

    public static boolean isDateFormatValid(String dOb){
        return Pattern.matches("\\d{2}/\\d{2}/\\{4}", dOb);
    }

    //---------------TASK-6---------------

    public static boolean isEmailFormatValid(String email){
        return Pattern.matches("[\\w]{2,}@" + "[\\w]{2,}\\."+
                "[\\w]{2,}", email);
    }

}
