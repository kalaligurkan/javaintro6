package homeworks;

public class Homework20 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");
        int[] numbers = {1, 5, 10};
        boolean evenOdd = true;
        System.out.println(sum1(numbers, evenOdd));

        System.out.println("\n---------------TASK-2---------------\n");
        System.out.println(sumDigitsDouble("Java"));
        System.out.println(sumDigitsDouble("ab12"));

        System.out.println("\n---------------TASK-3---------------\n");
        System.out.println(countOccurrence("Hello", "World"));
        System.out.println(countOccurrence("Can I can a can", "anc"));
    }

    //---------------TASK-1---------------
    public static int sum1(int[] arr, boolean evenOrOdd){
        int count = 0;
        for (int i : arr) {
            if(evenOrOdd) {
                if (i % 2 == 0) {
                    count++;
                }
            }
                else {
                    if (i % 2 != 0){
                        count++;
                    }
                }
            }
        return count;
        }

    //---------------TASK-2---------------
    public static int sumDigitsDouble(String str){
        int sum = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if(Character.isDigit(c)){
                int digit = Character.getNumericValue(c);
                if(digit >= 0){
                    sum += digit;
                }
            }
        }
        if(sum > 0){
            return sum * 2;
        }
        else {
            return -1;
        }
    }

    //---------------TASK-3---------------

    public static int countOccurrence(String firstWord, String secondWord) {
      int countOfFirst = firstWord.length();
      int countOfSecond = secondWord.length();

      String totalOfFirst = firstWord.replace("\\s", "").toLowerCase();
      String totalOfSecond= secondWord.replace("\\s", "").toLowerCase();

      String[] words = totalOfSecond.split("\\s");
      int count = 0;

        for (String word : words) {
            if(word.contains(totalOfFirst)){
                count++;
            }
        }
        return count;

        }


}


