package homeworks;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Homework10 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        String words1 = "      Java is fun       ";
        String words2 = "Selenium is the most common UI automation tool.   ";
        System.out.println(countWords(words1));
        System.out.println(countWords(words2));

        System.out.println("\n---------------TASK-2---------------\n");

        String words3 = "TechGlobal is a QA bootcamp";
        String words4 = "QA stands for Quality Assurance";
        System.out.println(countA(words3));
        System.out.println(countA(words4));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println(countPos(new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67))));
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123))));

        System.out.println("\n---------------TASK-4---------------\n");

        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3))));

        System.out.println("\n---------------TASK-5---------------\n");

        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"))));

        System.out.println("\n---------------TASK-6---------------\n");

        String words5 = "   I   am      learning     Java      ";
        String words6 = "Java  is fun    ";
        System.out.println(removeExtraSpaces(words5));
        System.out.println(removeExtraSpaces(words6));

        System.out.println("\n---------------TASK-7---------------\n");
        int[] num7 = {3, 0, 0, 7, 5, 10};
        int[] num8 = {6, 3, 2};
        System.out.println(Arrays.toString(add(num7, num8)));

        int[] num9 = {10, 3, 6, 3, 2};
        int[] num10 = {6, 8, 3, 0, 0, 7, 5, 10, 34};
        System.out.println(Arrays.toString(add(num9, num10)));

        System.out.println("\n---------------TASK-8---------------\n");

        int[] num11 = {10, -13, 5, 70, 15, 57};
        System.out.println(findClosestTo10(num11));

        System.out.println("\n End of the program");

    }

    //---------------TASK-1---------------

    public static int countWords(String str1) {

        return (str1.trim().split("\\s").length);
    }

    //---------------TASK-2---------------

    public static int countA(String str2) {

        return (str2.replaceAll("[^aA]", "").length());
    }

    //---------------TASK-3---------------

    public static int countPos(ArrayList<Integer> num3) {

        return (int) num3.stream().filter(e -> e > 0).count();
    }

    //---------------TASK-4---------------

    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> num4) {

        /*
         .distinct to remove the duplicates
         .collect(Collectors.toCollection())
          collect() method collects and Collectors.toCollection() takes argument
         and puts in a new ArrayList::new
         */

        ArrayList<Integer> dupNum = num4.stream().distinct().collect(Collectors.toCollection(ArrayList::new));

        return dupNum;
    }

    //---------------TASK-5---------------

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> str5){

        ArrayList<String> dupWord = str5.stream().distinct().collect(Collectors.toCollection(ArrayList::new));

        return dupWord;
    }

    //---------------TASK-6---------------

    public static String removeExtraSpaces(String str7){

        return str7.trim().replaceAll("\\s+", " ");

    }

    //---------------TASK-7---------------

    public static int[] add(int[] num7, int[] num8){

        /*
        put the longer in a new int[] to execute
        find the shorter and iterate to its length to find the sum
        then in the second fori add the rest of longer array to sum found above
        lastly print them sum + rest of longer array elements.
         */
        int[] sumOfArr = new int[Math.max(num7.length, num8.length)];

        for(int i = 0; i < Math.min(num7.length, num8.length); i++){
            sumOfArr[i] = num7[i] + num8[i];
        }
        for (int i = Math.min(num7.length, num8.length); i < Math.max(num7.length, num8.length) ; i++) {
            if(num7.length > num8.length)
                sumOfArr[i] = num7[i];
            else sumOfArr[i] = num8[i];
        }
        return sumOfArr;
    }

    //---------------TASK-8---------------

    public static int findClosestTo10(int[] arr) {
        int closest = Integer.MAX_VALUE;
        for (int num : arr) {
            if (num != 10 && Math.abs(num - 10) < Math.abs(closest - 10)) {
                closest = num;
            }
        }
        return closest;
    }








}


