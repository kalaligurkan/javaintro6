package homeworks;

import java.util.ArrayList;
import java.util.List;

public class Homework14 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");
        int num = 18;
        fizzBuzz1(num);

        System.out.println("\n---------------TASK-2---------------\n");
        System.out.println(fizzBuzz2(30));

        System.out.println("\n---------------TASK-3---------------\n");
        System.out.println(findSumNumbers("a1b2c3"));

        System.out.println("\n---------------TASK-4---------------\n");
        System.out.println(findBiggestNumber("ab110c045d"));

        System.out.println("\n---------------TASK-5---------------\n");
    }
    //---------------TASK-1---------------
        public static void fizzBuzz1(int num) {
            for (int i = 1; i <= num; i++) {
                if (i % (3 * 5) == 0) {
                    System.out.println("FizzBuzz");
                } else if (i % 3 == 0) {
                    System.out.println("Fizz");
                } else if (i % 5 == 0) {
                    System.out.println("Buzz");
                } else System.out.println(i);
            }
        }

        //---------------TASK-2---------------
        public static String fizzBuzz2(int num) {
                if (num % 3*5 == 0) {
                    return "FizzBuzz";
                } else if (num % 3 == 0) {
                    return "Fizz";
                } else if (num % 5 == 0) {
                    return "Buzz";
                } else {
                    return String.valueOf(num);
                }
            }

        //---------------TASK-3---------------
        public static int findSumNumbers(String str){
            int num = 0;
            int sum = 0;

            for (int i = 0; i < str.length(); i++) {
                if(Character.isDigit(str.charAt(i)))
                    num += num * str.charAt(i);
                else {
                    sum += num;
                    num = 0;
                }
            }

            return sum+num;
        }


    //---------------TASK-4---------------
         public static int findBiggestNumber(String str){
            int num = 0;
            int max = 0;

            for (int i = 0; i < str.length(); i++) {
                if(Character.isDigit(str.charAt(i)))
                num += num * str.charAt(i);
            else{
                max = Math.max(num, max);
            }
        }
        return num;

    }

        //---------------TASK-5---------------
       /* public static String countSequenceOfCharacters(String str){
        if(str.isEmpty()) return str;
        } */


}
