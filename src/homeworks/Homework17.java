package homeworks;


import java.util.Scanner;

public class Homework17 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");

        System.out.println(nthWord("I like programming language", 2));
        System.out.println(nthWord("QA stands for Quality Assurance", 4));

        System.out.println("\n---------------TASK-2---------------\n");

        System.out.println(isArmStrong(153));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println(reverseNumber(371));
    }

    //---------------TASK-1---------------
    public static String nthWord(String str, int num) {
       Scanner input = new Scanner(str);


        for (int i = 1; i < num; i++) {
            if (input.hasNext()) {
                input.next();
            } else {
                input.close();
                return "";
            }
        }
            if (input.hasNext()) {
                String nthWord = input.next();
                input.close();
                return nthWord;
            } else {
                input.close();
                return "";
            }
        }

    //---------------TASK-2---------------

    public static boolean isArmStrong(int num){
        int sum = 0;
        int backUpNum = num;

        while (num > 0){
            sum = sum + (num % 10) * (num % 10) * (num % 10) ;
            num = num / 10;
        }
        if(sum == backUpNum){
            return true;
        }
        else {
            return false;
        }
    }

    //---------------TASK-3---------------

    public static int reverseNumber(int num){
        int rev = 0;

        while(num != 0){
            rev = rev * 10 + num % 10;
            num = num / 10;
        }
        return rev;
    }
}











