package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");
        System.out.println(hasLowerCase("hello"));

        System.out.println("\n---------------TASK-2---------------\n");
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1, 1, 0, 10));
        System.out.println(noZero(numbers));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println("\n---------------TASK-4---------------\n");
        System.out.println(containsValue(new String[]{"abc", "def", "123", "Java", "Hello"}, "123"));

        System.out.println("\n---------------TASK-5---------------\n");

        System.out.println("\n---------------TASK-6---------------\n");
        String str = "Hello #$%^ World987";
        System.out.println(removeStringSpecialsDigits(str));

        System.out.println("\n---------------TASK-7---------------\n");
        String[] strArr = {"Hello#$%^", "World 456"};
        System.out.println(Arrays.toString(removeArraySpecialsDigits(strArr)));

        System.out.println("\n---------------TASK-8---------------\n");

        System.out.println("\n---------------TASK-9---------------\n");
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("xyz", "123", "#$%"))));


    }

    //---------------TASK-1---------------
    public static boolean hasLowerCase(String str){
        for (int i = 0; i < str.length(); i++) {
            if(Character.isLowerCase(str.charAt(i)))
                return true;
        }
        return false;
    }

    //---------------TASK-2---------------
   public static ArrayList<Integer> noZero(ArrayList<Integer> arr){
    ArrayList<Integer> result = new ArrayList<>();
       for (Integer num : arr) {
           if(num != 0)
               result.add(num);
       }
       return result;
   }

    //---------------TASK-3---------------
  /*  public static int[][] numberAndSquare(int[] array) {

    }  */

    //---------------TASK-4---------------
    public static boolean containsValue(String[] arr, String str){
        Arrays.sort(arr);
        return Arrays.binarySearch(arr, str) >= 0;
    }

    //---------------TASK-5---------------

    //---------------TASK-6---------------
    public static String removeStringSpecialsDigits(String str){
        return str.replaceAll("[^a-zA-Z\\s]", "");
    }

    //---------------TASK-7---------------
    public static String[] removeArraySpecialsDigits(String[] arr){
        String[] result = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i].replaceAll("[^a-zA-Z\\s]", "");
        }
        return result;
    }

    //---------------TASK-8---------------

    //---------------TASK-9---------------
    public static ArrayList<String> noXInVariables(ArrayList<String> strList) {
        ArrayList<String> result = new ArrayList<>();
        for (String element : strList) {
            if (!(element.equalsIgnoreCase("x") || element.equalsIgnoreCase("x") || element.toLowerCase().contains("x"))) {
                result.add(element.replace("x", "").replace("X", ""));
            }
        }
        return result;
    }






}
