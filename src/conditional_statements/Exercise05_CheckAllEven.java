package conditional_statements;

import java.util.Scanner;

public class Exercise05_CheckAllEven {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);

        System.out.println("Please enter 3 numbers?");

        int number1 = input.nextInt(), number2 = input.nextInt(), number3 = input.nextInt();

        if(number1 % 2 == 0 && number2 % 2 == 0 && number3 % 2 == 0){ // System.out.println(number1 % 2 == 0 && number2 % 2 == 0 && number3 % 2 == 0)
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }
    }
}
