package conditional_statements;

import java.util.Random;

public class Practice02 {
    public static void main(String[] args) {

        /*
        Requirement:
Write a program that generates a
random number between 0 and 50
(both 0 and 50 are included)
Print true if number is in between 10
and 25 (10 and 25 included)
Print false otherwise
         */

       Random r = new Random();

        int number = r.nextInt(51);
        System.out.println(number >=10 && number <=25);

       /* if (number <= 25 && number >= 10){
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }*/


        /*
        Requirement:
            Write a program that generates a random number
            between 1 and 100 (both 1 and 100 are included)
            Find which quarter and half is number in
            1st
             quarter is 1-25
            2
            nd
             quarter is 26-50
            3
            rd
             quarter is 51-75
            4
            th
             quarter is 76-100
            1
            st
             half is 1-50
            2
            nd
             half is 51-100
            Test data:
            34
            Expected result:
            34 is in the 1
            st
             half
            34 is in the 2
            nd
             quarter
         */

            Random random = new Random();

            int number1 = random.nextInt(100) + 1;
            System.out.println(number1);

            if( number1 >= 1 && number1 <=25) {
                System.out.println(number1 + " is in the 1st quarter");
                System.out.println(number1 + " is in the 1st half");
            }
            else if(number1 > 25 && number1 <=50) {
                System.out.println(number1 + " is in the 2st quarter");
                System.out.println(number1 + " is in the 1st half");
            }
            else if(number1 > 50 && number1 <=75) {
                System.out.println(number1 + " is in the 3st half");
                System.out.println(number1 + " is in the 2st half");
            }
            else {
                System.out.println(number1 + " is in the 4th quarter");
                System.out.println(number1 + " is in the 2st half");
            }


            /* Requirement:
                -Assume you are given a single character. (It will be hard-coded)
                -If given char is a letter, then print “Character is a letter”
                -If given char is a digit, then print “Character is a digit”
                USE ASCII TABLE for this task
                Test data:
                ‘v’
                Expected result:
                Character is a letter
                Test data:
                ‘5’
                Expected result:
                Character is a digit */


                char character = '5';

                if( (character >= 65 && character <= 90) || (character >= 97 && character <= 122))
                    System.out.println("Character is a letter");
                else if (character >= 48 && character <= 57)
                    System.out.println("Character is a digit");

    }
}
