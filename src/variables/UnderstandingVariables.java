package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {

        /*
        int numberOfStudents; ->  declared
        numberOfStudents = 100;  -> assigned
         */

        String name = "John";   // declaring and initializing the variable

        int age;        //declaring the variable without a value

         age = 25;     // initializing the variable


        // capital letters and lowercase letters are seen differently with Java
        double money = 5.5;
        double mOney = 4.5;
        double Money;

        double d1 = 10;  // printout is 10.0. this one is added

        System.out.println(d1);

        d1 = 15.5;  // this is also added in the jar
        System.out.println(d1);


        System.out.println(d1);  // you will see the latest in the next System.out.println(d1);





    }
}
