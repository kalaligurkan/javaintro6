package loops.practices;

import utilities.ScannerHelper;

public class Exercise04 {
    public static void main(String[] args) {
        String str = ScannerHelper.getString().toLowerCase();
        int counter = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = Character.toLowerCase(str.charAt(i));
            if(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') counter++;
        }
        System.out.println(counter);

    }
}
