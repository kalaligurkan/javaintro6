package loops;

import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {
        /*
Write a program that asks user to enter 2 different positive numbers
Print all the even numbers bt given numbers by user in ascending order
3, 10 ->
4
6
8
10

7, 2  ->
2
4
6


 */
       int input1 = ScannerHelper.getNumber();
       int input2 = ScannerHelper.getNumber();
        for (int i = Math.min(input1, input2); i <= Math.max(input1, input2) ; i++) {
            if(i % 2 ==0) System.out.println(i);
        }



    }
}
