package scanner_class;

import java.util.Scanner;  // 1. write Scanner and choose (java.util)

public class FirstScannerProgram {
    public static void main(String[] args){
        //.next method
        Scanner input = new Scanner(System.in);  //2. then write this. this is the key to create objects

        System.out.println("Please enter your name.");           //when you run this you cannot write anything under this sentence in the console.
         String name = input.next();                                  // if you write here you can type ONLY 1 word in the console

        System.out.println("The user's name is " + name);                // String name =    // this gives you your name from console
        input.nextLine();   // this goes to next line


        System.out.println("\n-----------------------------------------\n");

        //.nextLine method

        System.out.println("Please enter your first and last name.");
        String fullName = input.nextLine();   // if you write this you can type more than 1 word in the console

        System.out.println("The users full name is: " + fullName);

        //.nextInt method
        System.out.println("Please enter a number");
        int number = input.nextInt();
        System.out.println("The number you chose is: " + number);



    }
}
