package scanner_class;

import java.util.Scanner;

public class ScannerFirstAndLastName {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your first name?");
        String firstName = input.next();


        System.out.println("Please enter your last name?");
        String lastName = input.next();
        input.nextLine();    // this to prevent error in the system

        System.out.println("Your full name is " + firstName + " " + lastName);


        /*
        Scanner scanner = new Scanner(System.in);

        String fName, lName;

        System.out.println("Please enter your name?");
         fName = scanner.next();

         System.out.println("Please enter your name?");
         lName = scanner.next();

         System.out.println("Your full name is " + fName + " " + lName);
         */

    }









}
