package scanner_class;

import java.util.Scanner;

public class AkinScanner {
    public static void main(String[] args) {


        // 1. Create a Scanner object

        // dataType variableName = value;

        //.next is going to read ONLY ONE WORD!
        // Both next() and nextLine() are used to read strings from the users
        /*
         DIFFERENCE
          next() -> gets  only one word
          nextLine()  -> whole line
          nextInt() -> next int
         */

        String fName, lName;              // declare

        Scanner inputReader = new Scanner(System.in);


        System.out.println("Please enter your name:");
        fName = inputReader.next();  // John

        System.out.println("Please enter your last name:");
        lName = inputReader.next();  // Doe

        System.out.println("Your full name is = " + fName + " " + lName);






    }
}
