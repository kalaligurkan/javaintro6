package scanner_class;

import java.util.Scanner;

public class Recap01 {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);

        /*
        Write a program that asks user to enter their firstName, address, favNumber

        Then, print them all in a way as below
        {firstname}'s address is {address} and their fav number is {favNumber}.

        FirstName = John
        Address = Chicago IL
        Fav Number = 7;

        John's address is Chicago IL and their fav number is 7.
         */




        System.out.println("What is your name?" );
        String fName = input.next();
        input.nextLine();  // if you don't write this here, then you will not be able to go next address because next() sees a huge blank after fName
                            // empty line to skip the problem of Scanner
        //System.out.println("What is your address?" );
        String address = input.nextLine();

        System.out.println("What is your fav number?" );
        int favNumber = input.nextInt();

        System.out.println(fName + "'s" + " " + "address is" + address + "and their" + "fav number is" + favNumber + "." );

    }
}
