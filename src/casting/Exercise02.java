package casting;

public class Exercise02 {
    public static void main(String[] args) {


        double price = 900;
        double dailySaveAmount = 50;


        System.out.println("You can buy the phone after " + (int) (price / dailySaveAmount) + " days.");
    }
}
