package gitDemo;

public class Demo {
    public static void main(String[] args) {
        String firstName = "Gurkan ";
        String lastName = "Kalali";

        System.out.println(firstName + lastName);

        String favColor = "Black";
        System.out.println("My favorite color is " + favColor);


        String favCar = "Audi";
        int year = 2023;
        System.out.println("My favorite car is " + favCar + " and the model year is " + year);


    }
}
