package string_methods;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise05 {
    public static void main(String[] args) {
        String word = ScannerHelper.getString();
        System.out.println(word.startsWith("A") || word.endsWith("a") && word.startsWith("E") || word.endsWith("e"));


    }
}
