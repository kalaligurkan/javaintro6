package string_methods;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
       /* Write a Java program that asks user to enter their favorite
        book name and quote

        And store answers of user in different Strings

        Finally, print the length of those Strings with proper
                messages */

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your favorite book and quote");
        String favBook = input.nextLine();
        String quote = input.nextLine();

        System.out.println(favBook.length());
        System.out.println(quote.length());
    }
}
