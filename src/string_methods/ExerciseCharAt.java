package string_methods;

import utilities.ScannerHelper;

import java.util.Scanner;

public class ExerciseCharAt {
    public static void main(String[] args) {
        String str = "TechGlobal"; // last index is 9 | length is 10
        String str2 = "Hello World"; // last index is 10 | length is 11
        String str3 = "I really love java"; // last index is 17 | length is 18
        System.out.println(str.charAt(4)); // G

        //last character
        //way 1

        System.out.println(str.charAt(9));

        //way 2
        System.out.println(str.charAt(str.length()-1));
        System.out.println(str2.charAt(str2.length()-1));



        String userString = ScannerHelper.getString();
        System.out.println(userString.charAt(userString.length()-1));
    }
}
