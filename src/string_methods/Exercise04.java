package string_methods;

public class Exercise04 {
    public static void main(String[] args) {

        /*assume you have the string "I go to TechGlobal"
        extract every word from the string into other strings and print them out on different lines

        expected output:
        I
        go
        to
        TechGlobal */
        String str = "I go to TechGlobal";

        System.out.println(str.substring(0,1)); // text.charAt(0)
        System.out.println(str.substring(2, 4));
        System.out.println(str.substring(5, 7));
        System.out.println(str.substring(8));
    }
}
