package operators.arithmetic_operators;

public class DivisionPractices {
    public static void main(String[] args) {

        int i1 = 5, i2 = 2;
        double d1 = 5, d2 = 2;

        int division1 = i1 / i2;
        double division2 = d1 / d2;


        double num1 = 15, num2 = 2;
        // I also can write double result = num1 / num2;

        System.out.println("The division of " + num1 + " by " + num2 + " = " + num1 / num2);

    }
}
