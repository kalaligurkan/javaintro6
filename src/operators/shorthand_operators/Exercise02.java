package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        /*
                Write a Java program that asks user to enter their balance and one day transactions.
            Subtract each transaction from balance and return new balance using shorthand operator


            Requirements:
            Use Scanner class to read input from user

            Test data:
            Balance = $100.00
            1st transaction = $25.75
            2nd transaction =  $12.50
            3rd transaction = $7.25

            Expected output:
            Balance after 1st transaction = $74.25
            Balance after 2nd transaction = $61.75
            Balance after 3rd transaction = $54.5
             */
        System.out.println("Hey user, please enter your balance");
        double balance = input.nextDouble();
        System.out.println("The initial balance = $" + balance);

        System.out.println("What is the first transaction amount?");
        double firstTrns = input.nextDouble();

        balance -= firstTrns;

        System.out.println("The balance after first transaction = $" + balance);

        System.out.println("What is the second transaction amount?");
        double secondTrns = input.nextDouble();  // balance -= input.nextDouble();

        balance -= secondTrns;

        System.out.println("The balance after second transaction = $" + balance);
        // System.out.println("The balance after second transaction = $" + (balance -= input.nextDouble()));

        System.out.println("What is the third transaction amount?");
        double thirdTrns = input.nextDouble();

        balance -= thirdTrns;

        System.out.println("The balance after third transaction = $" + balance);
        /*Scanner inputReader = new Scanner(System.in);

System.out.println("Hey user, please enter your balance: ");
double balance = inputReader.nextDouble();

System.out.println("The initial balance = $" + balance);

System.out.println("What is the first transaction amount?");
System.out.println("The balance after first transaction = $" + (balance -= inputReader.nextDouble()));

System.out.println("What is the second transaction amount?");
System.out.println("The balance after second transaction = $" + (balance -= inputReader.nextDouble()));

System.out.println("What is the third transaction amount?");
System.out.println("The balance after third transaction = $" + (balance -= inputReader.nextDouble()));
*/


    }
}
