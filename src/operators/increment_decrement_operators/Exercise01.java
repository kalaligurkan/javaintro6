package operators.increment_decrement_operators;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number:");
        int number = input.nextInt();

        int num = 3;

        System.out.println(num + " * " + 1 + " = " + num * 1);
        System.out.println(num + " * " + 2 + " = " + num * 2);
        System.out.println(num + " * " + 3 + " = " + num * 3);
        System.out.println(num + " * " + 4 + " = " + num * 4);
        System.out.println(num + " * " + 5 + " = " + num * 5);

        /*Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number: ");
        int number = scanner.nextInt();
        int index = 1;

        System.out.println(number + " * " + index + " = " + number * index++);
        System.out.println(number + " * " + index + " = " + number * index++);
        System.out.println(number + " * " + index + " = " + number * index++);
        System.out.println(number + " * " + index + " = " + number * index++);
        System.out.println(number + " * " + index + " = " + number * index++);
*/




    }
}
