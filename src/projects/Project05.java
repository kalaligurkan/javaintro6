package projects;

import utilities.RandomGenerator;
import utilities.ScannerHelper;


public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        String str= ScannerHelper.getSentence();

        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') counter++;
        }
        counter++;
        System.out.println("This sentence has " + counter + " words.");



        System.out.println("\n---------------TASK-2---------------\n");

        int randomNum1 = RandomGenerator.getRandomNumber(0, 25);
        int randomNum2 = RandomGenerator.getRandomNumber(0, 25);
        System.out.println(randomNum1);
        System.out.println(randomNum2);

        String str1 = "";

        for (int i = Math.min(randomNum1,randomNum2); i <= Math.max(randomNum1,randomNum2); i++) {
            if(i % 5 !=0) str1 += i + " - ";
        }
        System.out.println(str1.substring(0,str1.length() - 3));


        System.out.println("\n---------------TASK-3---------------\n");

        String str2 = ScannerHelper.getString();
        int counter1 = 0;

        if (str2.length() == 0 ) {
            System.out.println("This sentence does not have any characters.");
        }
        else {
            for (int i = 0; i < str2.length(); i++) {
                if (str2.charAt(i) == 'a' || str2.charAt(i) == 'A') counter1++;
            }
            System.out.println("This sentence has " + counter1 + " a or A letters.");
        }

        System.out.println("\n---------------TASK-4---------------\n");

        String str3 = ScannerHelper.getString();

        boolean isPalindrome = true;

        if (str3.length() < 1) {
            System.out.println("This word does not have 1 or more characters.");
        } else {
            for (int i = 0, j = str3.length() - 1; i <= str3.length() / 2; i++, j--) {
                if (str3.charAt(i) == str3.charAt(j)) {
                    isPalindrome = false;
                    break;
                }
            }
            if (isPalindrome) System.out.println("This word is not a palindrome");
            else System.out.println("This word is a palindrome");
        }


        System.out.println("\n---------------TASK-5---------------\n");

        for (int i = 1; i <= 9; i++) {
            for (int s = 1; s <= 9 - i; s++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= i * 2 - 1; j++) {
                System.out.print("*");

            }
            System.out.println();
        }


    }

}




