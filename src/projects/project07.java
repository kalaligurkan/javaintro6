package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class project07 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        System.out.println(Arrays.toString(new int[]{countMultipleWords(new String[]{"foo", "", " ", "foo bar", "java is fun", " ruby "})}));

        System.out.println("\n---------------TASK-2---------------\n");

        System.out.println(removeNegatives(new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15))));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println(validatePassword("Abcd1234"));

        System.out.println("\n---------------TASK-4---------------\n");

        System.out.println(validateEmailAddress("abcd@gmail.com"));

    }

    //---------------TASK-1---------------

            public static int countMultipleWords(String[] list){
                int counter = 0;
                for (String str : list) {
                    if(str.trim().contains(" ")) counter++;
                }
                return counter;
            }

    //---------------TASK-2---------------

            public static ArrayList<Integer> removeNegatives(ArrayList<Integer> numbers){
                numbers.removeIf(element -> element < 0);
                return numbers;
            }

    //---------------TASK-3---------------

            public static boolean validatePassword(String password){
                int upperL = 0, lowerL = 0, digit = 0, special = 0;

                if((password.length() >= 8 && password.length() <= 16) && !password.contains(" ")){
                    for(char c : password.toCharArray()){
                        if(Character.isUpperCase(c)) upperL++;
                        else if (Character.isLowerCase(c)) lowerL++;
                        else if (Character.isDigit(c)) digit++;
                        else special++;
                    }
                }
                return upperL > 0 && lowerL > 0 && digit > 0 && special > 0;
            }

    //---------------TASK-4---------------

            public static boolean validateEmailAddress(String email){
                int counterAt = 0, counterDot = 0;

                if(!(email.contains(" ")) && email.substring(0, email.indexOf("@")).length() >=2 &&
                email.substring(email.indexOf("@"), email.indexOf(".")).length() > 2 && email.substring(email.indexOf(".")).length() > 2){
                    for (int i = 0; i < email.length(); i++) {
                        if(email.charAt(i) == '@') counterAt++;
                        if(email.charAt(i) == '.') counterDot++;
                    }
                    return counterDot == 1 && counterAt == 1;
                }
                return false;
            }

}
