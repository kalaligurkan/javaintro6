package projects;

public class Project03 {
    public static void main(String[] args){

        System.out.println("\n---------------TASK-1---------------\n");

        String s1 = "24", s2 = "5";

        System.out.println("The sum of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) + Integer.parseInt(s2)));
        System.out.println("The subtraction of " + s1 + " and " + s2 + " = " + + (Integer.parseInt(s1) - Integer.parseInt (s2)));
        System.out.println("The division of " + s1 + " and " + s2 + " = " + + (Double.parseDouble(s1) / Double.parseDouble(s2)));
        System.out.println("The multiplication of " + s1 + " and " + s2 + " = " + + (Integer.parseInt(s1) * Integer.parseInt(s2)));
        System.out.println("The remainder of " + s1 + " and " + s2 + " = " + + (Integer.parseInt(s1) % Integer.parseInt (s2)));


        System.out.println("\n---------------TASK-2---------------\n");


        int randomNumber = ((int) (Math.random() * 35) + 1);

        if(randomNumber == 2 || randomNumber == 3 || randomNumber == 5 || randomNumber == 7 || randomNumber == 11 || randomNumber == 13 ||
                randomNumber == 17 || randomNumber == 19 || randomNumber == 23 || randomNumber == 29 || randomNumber == 31)
            System.out.println(randomNumber + " IS A PRIME NUMBER");

        else System.out.println(randomNumber + " IS NOT A PRIME NUMBER");




        System.out.println("\n---------------TASK-3---------------\n");

        int randomNum1 = ((int) (Math.random() * 50) + 1);
        int randomNum2 = ((int) (Math.random() * 50) + 1);
        int randomNum3 = ((int) (Math.random() * 50) + 1);

        int lowest = (Math.min(Math.min(randomNum1, randomNum2), randomNum3));
        int greatest = (Math.max(Math.max(randomNum1, randomNum2), randomNum3));
        int middle = randomNum1 + randomNum2 + randomNum3 - lowest - greatest;

        System.out.println("Lowest number is = " + lowest);
        System.out.println("Middle number is = " + middle);
        System.out.println("Greatest number is = " + greatest);



        System.out.println("\n---------------TASK-4---------------\n");

        char c = 't';

        if ((c >= 0 && c <= 65) || (c >= 91 && c <= 96) || (c >= 123 && c <= 127))
            System.out.println("Invalid character detected!!!");
        else if (c >= 97 && c <= 122)
            System.out.println("The letter is lowercase");
        else System.out.println("The letter is uppercase");



        System.out.println("\n---------------TASK-5---------------\n");

        char c1 = 'a';

        if ((c1 >= 0 && c1 <= 65) || (c1 >= 91 && c1 <= 96) || (c1 >= 123 && c1 <= 127))
        System.out.println("Invalid character detected!!!");
        else if ((c1 == 65) || (c1 == 69) || (c1 == 73) || (c1 == 79) || (c1 == 85) || (c1 == 97) || (c1 == 101) || ( c1 == 105)
                || (c1 == 111) || (c1 == 117))
            System.out.println("The letter is vowel");
        else System.out.println("The letter is consonant");


        System.out.println("\n---------------TASK-6---------------\n");

        char c2 = '(';

        if((c2 >= 65 && c2 <= 90) || (c2 >= 97 && c2 <= 122) || (c2 >= 48 && c2 <= 57))
            System.out.println("Invalid character detected!!!");
        else System.out.println("Special character is = " + c2);


        System.out.println("\n---------------TASK-7---------------\n");

        char c3 = ')';

        if((c3 >= 65 && c3 <= 90) || (c3 >= 97 && c3 <= 122))
            System.out.println("Character is a letter");
        else if(c3 >= 48 && c3 <= 57)
            System.out.println("Character is a digit");
            else System.out.println("Character is a special character");


            System.out.println("\nEnd of the program");




    }
}
