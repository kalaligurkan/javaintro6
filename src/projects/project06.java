package projects;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class project06 {
    public static void main(String[] args) {

        //METHODS ARE BENEATH THE EXAMPLE EXECUTIONS

        System.out.println("\n---------------TASK-1---------------\n");

        int[] arr1 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(arr1);

        System.out.println("\n---------------TASK-2---------------\n");

        int[] arr2 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallest(arr2);

        System.out.println("\n---------------TASK-3---------------\n");

        int[] arr3 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(arr3);

        System.out.println("\n---------------TASK-4---------------\n");

        int[] arr4 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallest(arr4);

        System.out.println("\n---------------TASK-5---------------\n");

        String[] str1 = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElementsInAnArray(str1);

        System.out.println("\n---------------TASK-6---------------\n");

        String[] str2 = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementInAnArray(str2);
    }
                            //---------------TASK-1---------------

    public static void findGreatestAndSmallestWithSort(int[] arr1) {
        Arrays.sort(arr1);
        int smallest1 = arr1[0];
        int greatest1 = arr1[arr1.length - 1];
        System.out.println("Smallest = " + smallest1);
        System.out.println("Greatest = " + greatest1);

    }
                            //---------------TASK-2---------------

    public static void findGreatestAndSmallest(int[] num) {
        if (num.length >= 1) {
            int smallest = num[0];
            int greatest = num[0];
            for (int element : num) {
                if (element < smallest) smallest = element;
                if (element > greatest) greatest = element;
            }
            System.out.println("Smallest = " + smallest);
            System.out.println("Greatest = " + greatest);
        } else System.out.println("Array is empty");
    }
                        //---------------TASK-3---------------

    public static void findSecondGreatestAndSmallestWithSort(int[] num) {
        Arrays.sort(num);
        int min = num[0];
        int max = num[num.length - 1];

        int secondMin = Integer.MAX_VALUE;
        int secondMax = Integer.MIN_VALUE;

        for (int j : num) {
            if (j != max && j > secondMax) secondMax = j;
            if (j != min && j < secondMin) secondMin = j;
        }
        System.out.println("Second Smallest = " + secondMin);
        System.out.println("Second Greatest = " + secondMax);
    }

                        //---------------TASK-4---------------

    public static void findSecondGreatestAndSmallest(int[] number){
        int firstSmall = number[0];
        int firstGreat = number[0];
        for (int j : number) {
            if (j > firstGreat) firstGreat = j;
            if (j < firstSmall) firstSmall = j;
        }
        int smallSecond = Integer.MAX_VALUE;
        int greatSecond = Integer.MIN_VALUE;
        for (int j : number) {
            if (j != firstSmall && j < smallSecond) smallSecond = j;
            if (j != firstGreat && j > greatSecond) greatSecond = j;
        }
        System.out.println("Second smallest = " + smallSecond);
        System.out.println("Second greatest = " + greatSecond);
    }

                        //---------------TASK-5---------------

    public static void findDuplicatedElementsInAnArray(String[] list) {
        String duplicates = "";
        for (int i = 0; i < list.length; i++) {
            for (int j = i + 1; j < list.length; j++) {
                if (!duplicates.contains(list[i]) && list[i].equals(list[j])) {
                    duplicates += list[i] + " ";
                    System.out.println(list[i]);
                    break;
                }
            }
        }
    }

                        //---------------TASK-6---------------

    public static void findMostRepeatedElementInAnArray(String[] list){
        int number = 0;
        int repeatCount = 0;
        int counter = 0;
        for(int i = 0; i < list.length; i++) {
            for (String s : list) {
                if (list[i] == s) counter++;
            }
            if (repeatCount < counter) {
                repeatCount = counter;
                number = i;
            }
            counter = 0;
        }

        System.out.println(list[number]);
    }


}
