package projects;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("\n---------------TASK-1---------------\n");

        System.out.println(findClosestDistance(new int[]{10, -5, 20, 50, 100}));

        System.out.println("\n---------------TASK-2---------------\n");

        System.out.println(findSingleNumber(new int[]{5, 3, -1, 3, 5, 7, 7, 5, 3, -1, 3, 5, 7, 7, 6}));

        System.out.println("\n---------------TASK-3---------------\n");

        System.out.println(findFirstUniqueCharacter("ababc"));

        System.out.println("\n---------------TASK-4---------------\n");

        System.out.println(findMissingNumber(new int[]{2, 3, 1, 5, 4, 7})); // 4
    }


    //---------------TASK-1---------------

    public static int findClosestDistance(int[] num) {

        if (num.length < 2)
            return -1;
        Arrays.sort(num);
        int difference = Integer.MAX_VALUE;
        for (int i = 0; i < num.length - 1; i++) {
            int distance = num[i + 1] - num[i];
            difference = Math.min(distance, difference);
        }
        return difference;
    }

    //---------------TASK-2---------------

    public static int findSingleNumber(int[] num) {

        Arrays.sort(num);
        if (num.length == 1)
            return num[0];
        for (int i = 0; i < num.length - 1; i++) {
            if (num[i] != num[i + 1])
                return num[i];
            if (num[i] == num[i + 1]) i++;
        }
        return num[num.length - 1];
    }

    //---------------TASK-3---------------

    public static char findFirstUniqueCharacter(String str) {

        for(int i =0; i < str.length(); i++){
        char c = str.charAt(i);
            if(str.indexOf(c) == i && str.lastIndexOf(c) == i){
                return c;
            }
        }
        return ' ';
    }

    //---------------TASK-4---------------

    public static int findMissingNumber(int[] num) {

        Arrays.sort(num);
        for (int i = 0; i < num.length-1; i++) {
            int currentNum = num[i] + 1;
            int expectedNum = num[i + 1];
            if (expectedNum != currentNum)
                return currentNum;
        }
        return 0;
    }
}

