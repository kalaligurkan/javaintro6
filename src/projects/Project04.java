package projects;

import utilities.ScannerHelper;

public class Project04 {
    public static void main(String[] args) {

      System.out.println("\n---------------TASK-1---------------\n");

        String str = ScannerHelper.getString();

        if(str.length() < 8) System.out.println("This String does not have 8 characters");
        else System.out.println((str.substring(str.length()-4)  + str.substring(4, str.length()-4)+ str.substring(0, 4)));


        System.out.println("\n---------------TASK-2---------------\n");

        String strSentence = ScannerHelper.getSentence();
        if (strSentence.contains(" "))
            System.out.println( strSentence.substring(strSentence.lastIndexOf(" ")+1)
                    + strSentence.substring(strSentence.indexOf(" "), strSentence.lastIndexOf(" ")+1)
                      + strSentence.substring(0, strSentence.indexOf(" ")));
        else System.out.println("This sentence does not have 2 or more words to swap");


        System.out.println("\n---------------TASK-3---------------\n");

        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        str1 = str1.replace("stupid", "nice");
        str2 = str2.replace("idiot", "nice");
        str3 = str3.replace("stupid", "nice");
        str3 = str3.replace("idiot", "nice");


        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);


        System.out.println("\n---------------TASK-4---------------\n");

        String name = ScannerHelper.getFirstName();
        if(name.length() < 2) System.out.println("Invalid input!!!");
        else if(name.length() % 2 == 0) System.out.println(name.substring(name.length()/2-1, name.length()-2));
        else System.out.println(name.substring(name.length()/2, name.length()-2));


        System.out.println("\n---------------TASK-5---------------\n");

        String country = ScannerHelper.getFavCountry();
        if(country.length()<5) System.out.println("Invalid input!!!");
        else System.out.println(country.substring(2, country.length()-2));


        System.out.println("\n---------------TASK-6---------------\n");

        String address = ScannerHelper.getAddress();
        address = address.replace('a', '*');
        address = address.replace('A', '*');
        address = address.replace('e', '#');
        address = address.replace('E', '#');
        address = address.replace('i', '+');
        address = address.replace('I', '+');
        address = address.replace('u', '$');
        address = address.replace('U', '$');
        address = address.replace('o', '@');
        address = address.replace('O', '@');
        System.out.println(address);



    }
}
