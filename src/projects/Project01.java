package projects;


public class Project01 {
    public static void main(String[] args) {

        System.out.println("\n---------------TASK-1---------------\n");

        String name = "Gurkan";
        System.out.println("My name is = " + name);

        System.out.println("\n---------------TASK-2---------------\n");

        char nameCharacter1 = 'G';
        char nameCharacter2 = 'u';
        char nameCharacter3 = 'r';
        char nameCharacter4 = 'k';
        char nameCharacter5 = 'a';
        char nameCharacter6 = 'n';
        System.out.println("Name letter 1 is = " +nameCharacter1);
        System.out.println("Name letter 2 is = " +nameCharacter2);
        System.out.println("Name letter 3 is = " +nameCharacter3);
        System.out.println("Name letter 4 is = " +nameCharacter4);
        System.out.println("Name letter 5 is = " +nameCharacter5);
        System.out.println("Name letter 6 is = " +nameCharacter6);

        System.out.println("\n---------------TASK-3---------------\n");

        String myFavMovie = "Into The Wild";
        String myFavSong = "Singularity by Steve Aoki";
        String myFavCity = "Edinburgh";
        String myFavActivity = "Reading";
        String myFavSnack = "Chocolate";
        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite song is = " + myFavSong);
        System.out.println("My favorite city is = " + myFavCity);
        System.out.println("My favorite activity is = " + myFavActivity);
        System.out.println("My favorite snack is = " + myFavSnack);

        System.out.println("\n---------------TASK-4---------------\n");

        int myFavNumber = 13;
        int numberOfStatesIVisited = 2;
        int numberOfCountriesIVisited = 4;
        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("The number of states I have visited = " + numberOfStatesIVisited);
        System.out.println("The number of countries I have visited = " + numberOfCountriesIVisited);

        System.out.println("\n---------------TASK-5---------------\n");

        boolean amIAtSchoolToday = false;
        System.out.println("I am at school today = " + amIAtSchoolToday);

        System.out.println("\n---------------TASK-6---------------\n");

        boolean isWeatherNiceToday = false;
        System.out.println("Weather is nice today = " + isWeatherNiceToday);




    }
}
