import java.util.LinkedHashMap;
import java.util.Map;

public class exercise {
    public static void main(String[] args) {
        /**
         * Requirement:
         * -Student is responsible to create a main method and
         * test their method.
         * -Write a method that takes an array of String and
         * returns the count of each unique element in the array
         * as a Map
         * Test Data
         * :
         * [“Apple”, “Apple”, “Orange”, “Apple”, “Kiwi”]
         * Expected
         * :
         * {Apple=3, Orange=1, Kiwi=1}
         */

    }

    public static Map<String, Integer> countElements(String[] arr) {
        Map<String, Integer> countfruits = new LinkedHashMap<>();
        for (String e : arr) {
            if (countfruits.containsKey(e)) {
                countfruits.put(e, countfruits.get(e) + 1);
            }
                else countfruits.put(e, 1);
            }
            return countfruits;
        }
    }
