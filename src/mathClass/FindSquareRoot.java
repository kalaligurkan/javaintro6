package mathClass;

public class FindSquareRoot {
    public static void main(String[] args) {
        // 5^2 == 25
        // square root of 25 == 5
        // it accepts double

        double d = 25;

        System.out.println(Math.sqrt(d)); //5.0
        System.out.println(Math.sqrt(25.0 - 5.0)); // 5.0
    }
}
