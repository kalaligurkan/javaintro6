package mathClass;

public class FindAbs {
    public static void main(String[] args) {
        // takes one int as argument (this is the number inside parentheses) and returns one argument in positive
        // -35  ->  |-35|  == 35
        // abs can take int, float, double

        int num = -100;

        System.out.println(Math.abs(num)); // converts negative number to positive
        System.out.println(Math.abs(50-55)); // 5
        System.out.println(Math.abs(5)); // 5
        System.out.println(Math.abs(-5.768349327492));
    }
}
