package mathClass;

import java.util.Scanner;

public class FindMin {
    public static void main(String[] args) {
        Scanner inputReader = new Scanner(System.in);

        System.out.println("Please enter 2 numbers;");
        int num1 = inputReader.nextInt();
        int num2 = inputReader.nextInt();

        System.out.println("The min of the given numbers is = " + Math.min(num1, num2));


        System.out.println("Please enter 3 numbers:");
        int n1 = inputReader.nextInt();
        int n2 = inputReader.nextInt();
        int n3 = inputReader.nextInt();

        System.out.println("The min of " + n1 + ", " + n2 + ", " + n3 + " is:" + (Math.min(Math.min(n1, n2), n3)));

        /*other way
        System.out.println("Please enter 3 numbers");
        int number1 = inputReader.nextInt();
        int number2 = inputReader.nextInt();
        int number3 = inputReader.nextInt();

        int min = Math.min(number1, Math.min(number2,number3));

        System.out.println("The min of " + number1 + ", " + number2 + ", " + number3 + " is: " + min);
         */


    }
}
