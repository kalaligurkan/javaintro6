package mathClass;

import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {

        System.out.println("\n----------Task1----------\n");

        int randomNumber = (int)(Math.random() * 51); // int randomNumber2 = (int)Math.round(Math.max() * 50);
        int result =  randomNumber * 5;

        System.out.println("The random number * 5 = " + result);

        System.out.println("\n----------Task2----------\n");

        /*
         int randomNumber = (int) Math.round(Math.random() * 10); 0 - 9 -> False
         int randomNumb = (int)(Math.random() * 11); 0 - 10             -> False
         int randomNumb = (int)(Math.random() * 10) + 1; 0 - 9 -> 1 - 11 -> False
         int random = (int) (Math.random() * 10 + 1);                   -> True
         */

        int num1 = (int) (Math.random() * 10 + 1);
        int num2 = (int) (Math.random() * 10 + 1);

        System.out.println("Min number = " + Math.min(num1, num2));
        System.out.println("Max number = " + Math.max(num1, num2));
        System.out.println("Difference = " + Math.abs(num1 - num2));


        System.out.println("\n----------Task3----------\n");

        // numbers 50 to 100 included

        int random = (int)((Math.random() * 51) + 50); // int random = (int)(Math.round(Math.random() * 50) + 50);
        System.out.println("The random number & 10 = " + random % 10 );

        System.out.println("\n----------Task3----------\n");

        Scanner inputReader = new Scanner(System.in);



        System.out.println("Please enter 5 numbers btw 1-10:");

        int number1 = inputReader.nextInt();
        int number2 = inputReader.nextInt();
        int number3 = inputReader.nextInt();
        int number4 = inputReader.nextInt();
        int number5 = inputReader.nextInt();

        /*
        int number1 = inputReader.nextInt() * 5;
        int number2 = inputReader.nextInt() * 4;
        int number3 = inputReader.nextInt() * 3;
        int number4 = inputReader.nextInt() * 2;
        int number5 = inputReader.nextInt() * 1;

        System.out.println(number1 + number2 + number3 + number4 + number5);
         */

        System.out.println((number1 * 5) + (number2 * 4) + (number3 * 3) + (number4 * 2) + (number5));










    }
}
