package mathClass;

public class FindMax {
    public static void main(String[] args) {
        // finding the max of 2 numbers
        int num1 = 10;
        int num2 = 15;

        int max = Math.max(num1, num2);

        System.out.println(max);

        // finding the max of 4 numbers

        int number1 = 2;
        int number2 = 8;
        int number3 = 5;
        int number4 = 18;

        // max between number1 and number2 = 8
        // max between number3 and number4 = 18

        int max1 = Math.max(number1, number2);
        int max2 = Math.max(number3, number4);

        System.out.println(Math.max(max1, max2));

        // finding the max of 3 numbers

        int n1 =-30;
        int n2 = -40;
        int n3 = 0;

        int max3 = Math.max(n1, n2);

        System.out.println(Math.max(max3, n3));

        /// finding the max of 5 numbers


        // Math.max(Math.max(a,b), Math.max(c,d)) = the max of the first 4 numbers
        //once 4 numaranin max al sonra besinciyi ekle hepsinin max al

        //Finding the max of 5 numbers
        int a = 5;
        int b = 10;
        int c = 50;
        int d = 189;
        int e = 12;

        //Math.max( Math.max(a,b), Math.max(c,d)) == the max of the first 4 numbers
        max1 = Math.max(a, b);//(5, 10) -> 10
        max2 = Math.max(c, d);//(50, 189) -> 189
        int maxNew = Math.max(max1, max2);//(10, 189) -> 189
        int finalMax = Math.max(max3, e);//(189, 12) -> 189

        System.out.println(finalMax);




        /* other way
        System.out.println(Math.max(Math.max(Math.max(a,b), Math.max(c,d)), e));  */







    }

}
