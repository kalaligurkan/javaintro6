package mathClass;

public class Random {
    public static void main(String[] args) {

        System.out.println(Math.random()); // generates random number from 0.0 to 1.0

        System.out.println(Math.random() * 10); // generates random number from 0.0 to 10.0
        System.out.println(Math.random() * 20); // generates random number from 0.0 to 20.0
        System.out.println(Math.random() * 11); // generates random number from 0.0 to 11.0 (10.0 included)
        System.out.println(Math.round(Math.random() * 10)); // generates random number from 0.0 to 10.0 included and rounds it up and down
        System.out.println((int)(Math.random() * 10));  //generates random number from 0 to 9 where 10 is not included
        System.out.println(Math.round(Math.random() * 10) + 10); // generates random number from 10 to 20 included and rounds it up and down
        System.out.println(Math.round(Math.random() * 25)); // generate a random number from 0 to 25 where 25 is included; make it a whole number
        System.out.println((int)(Math.random() * 10) + 1); // generates random number from 1 to 10 where 10 is included. no 0 because it adds all the number +1
        



        System.out.println(Math.round(Math.random() * 10) + 10); // generates random number from 10 to 20 included and rounds it up and down

        /*
        get me a random number btw 17 53 (both inclusive)
        steps to produce a random number for given range
        1. Find how many in your range: biggest - smallest + 1
        53 - 17 + 1 -> 37

        2. Multiply your random result with the number
        (int) (Math.random() * 37) -> 0 and 36 (both inclusive)

        3. Add smallest number to your result
        (int) (Math.random() * 37) + 17
         */





    }
}
