package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {
        /*
        Floating numbers 10.5, 23.46543, 100000.23265679784

        float   -> 4 bytes
        double  -> 8 bytes

         */

        System.out.println("\n-----------floating numbers----------\n");

        float myFloat1 = 20.5F;  //you need to add F if there is decimal just like Long L
        double myDouble1 = 20.5;

        System.out.println(myFloat1);
        System.out.println(myDouble1);

        System.out.println("\n-----------floating numbers more----------\n");

        float myFloat2 = 10;
        double myDouble2 = 234235;

        System.out.println(myFloat2);  //10.0
        System.out.println(myDouble2);  //234235.0

        System.out.println("\n-----------floating numbers precision(the number after the dot)----------\n");

        float f1 = 32746.23423542352F;
        double d1 = 32746.23423542352352;

        System.out.println(f1);
        System.out.println(d1);
    }
}
