package primitives;

public class Booleans {
    public static void main(String[] args) {
        /*
        boolean     ->  1 bit

        true
        false
         */

        boolean b1 = true;
        boolean b2 = false;
        boolean b3 = true; // there is no 3rd option, only two options

        System.out.println(b1);  //true
        System.out.println(b2);  //false
        System.out.println(b3);  //true
    }
}
