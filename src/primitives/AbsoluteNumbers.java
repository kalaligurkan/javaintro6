package primitives;

public class AbsoluteNumbers {
    public static void main(String[] args) {
        /*
        Absolute numbers: 1, 5, -3, 32132145564
        byte -> -128 to 127 (both inclusive)
        short -> -32768 to 32767
        int
        long

        dataType variableName = value;
         */

        System.out.println("\n-------byte---------\n");
        byte myNumber = 45;
        System.out.println(myNumber); // 45


        System.out.println("The max value of byte = " + Byte.MAX_VALUE); //127
        System.out.println("The min value of byte = " + Byte.MIN_VALUE); //-128 These two come from library


        System.out.println("\n-------short---------\n");
        short numberShort = 150;

        System.out.println(numberShort);

        System.out.println("The max value of short = " + Short.MAX_VALUE);
        System.out.println("The min value of short = " + Short.MIN_VALUE);


        System.out.println("\n-------int---------\n");

        int numberInteger = 2000000;
        System.out.println(numberInteger);

        System.out.println("The max value of int = " + Integer.MAX_VALUE);
        System.out.println("The min value of int = " + Integer.MIN_VALUE);

        System.out.println("\n------long---------\n");
        long myBigNumber = 2147483648L;

        System.out.println(myBigNumber); //2147483648

        System.out.println("\n------long more info---------\n");

        long l1 = 5;
        long l2 = 238_797_976_465_41L; // We need to put L when the number is more than the capacity int. we can put underscore
        // to make it more readable but not comma

        System.out.println(l1);  //5
        System.out.println(l2);  //23879797646541








    }
}
