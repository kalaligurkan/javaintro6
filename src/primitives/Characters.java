package primitives;

public class Characters {
    public static void main(String[] args) {
        /*
        char   -> 2 bytes
        it is used to store a single character
        letter, digit, space, specials

        dataType variableName = 'value';
         */

        char c1 = 'A';
        char c2 = ' ';        //space is also a character

        System.out.println(c1);  //A
        System.out.println(c2);  //

        char myFavCharacter = 'W';
        System.out.println("My favorite char = " + myFavCharacter);  // he is going to ask like this, be readable
    }
}
